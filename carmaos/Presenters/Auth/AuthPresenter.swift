//
// Created by Eduardo Alvarez on 21/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class AuthPresenter {

    private let model: AuthModel

    var maxLength: Int { return model.maxLength }
    var name: String? {
        get { return model.name }
        set { model.name = newValue }
    }
    var phone: String? {
        get { return model.phone }
        set { model.phone = newValue }
    }
    var verificationID: String? {
        get { return model.verificationID }
        set { model.verificationID = newValue }
    }

    // Constructor
    init(model: AuthModel) {
        self.model = model
    }

    func sendVerificationSMS(phone: String, callBack: @escaping (String) -> () ) {
        model.sendVerificationSMS(phone: phone, callBack: callBack)
    }
}
//
// Created by Eduardo Alvarez on 21/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class VerifyCodePresenter {

    private let model: VerifyCodeModel!

    var maxLength: Int { return model.maxLength }
    var verificationID: String? { return model.verificationID }
    var uid: String? {
        get { return model.uid }
        set { model.uid = newValue }
    }
    var isNewUser: Bool {
        get { return model.isNewUser }
        set { model.isNewUser = newValue }
    }

    // Constructor
    init(model: VerifyCodeModel) {
        self.model = model
    }

    func confirmVerificationSMS(verificationID: String, verificationCode: String, callBack: @escaping (Bool, String, Bool) -> Void) {
        model.confirmVerificationSMS(
                verificationID: verificationID,
                verificationCode: verificationCode,
                callBack: callBack
        )
    }
}
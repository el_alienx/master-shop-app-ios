//
// Created by Eduardo Alvarez on 22/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class AuthCompletePresenter {

    private var model: AuthCompleteModel!

    var onAuthCompleteSegue: String? { return model.onAuthCompleteSegue }

    // Constructor
    init (model: AuthCompleteModel) { self.model = model }

    func loadProfile(callBack: @escaping (Bool) -> Void) {
        model.loadProfile(callBack: callBack)
    }
}
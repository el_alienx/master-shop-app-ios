//
// Created by Eduardo Alvarez on 23/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class OrderHistoryPresenter {

    private var model: OrderHistoryModel

    var orders: [[String: Any]] { return model.orders }

    // Constructor
    init(model: OrderHistoryModel) {
        self.model = model
    }

    func readOrders(callBack: @escaping () -> Void) {
        model.readOrders(callBack: callBack)
    }
    
    func formatStateMessage() -> String {
        return "Aquí podrás ver todas tus compras en \(Brand.store.capitalized)"
    }

    func showDate(date: Any) -> String {
        return StringHandler.formatDate(date: date, format: "EEEE dd MMMM, YYYY").capitalized
    }

    func showTotal(amount: Double) -> String {
        return StringHandler.formatPrice(amount: amount)
    }
}

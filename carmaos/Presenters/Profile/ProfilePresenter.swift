//
//  ProfilePresenter.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 21/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class ProfilePresenter {
    
    private let model: ProfileModel!

    var isLogged: Bool { return model.isLogged }
    var isOrderPlaced: Bool { return model.isOrderPlaced }
    var name: String? { return model.name }
    var menuOptions: [[String: String]] { return model.menuOptions }
    var latestOrder: [String: Any]? { return model.latestOrder }
    var authSegue: String { return model.authSegue }

    // Constructor
    init(model: ProfileModel) {
        self.model = model
    }

    func signOut() {
        model.signOut()
    }

    func readLatestOrder(callBack: @escaping (Bool) -> Void) {
        model.readLatestOrder(callBack: callBack)
    }
}

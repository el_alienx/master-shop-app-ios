//
// Created by Eduardo Alvarez on 23/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class AddressBookPresenter {

    private var model: AddressBookModel

    var segue: String { return model.segue }
    var addresses: [Address] { return model.addresses }

    // Constructor
    init(model: AddressBookModel) {
        self.model = model
    }

    func readAddresses(callBack: @escaping  (Bool) -> Void) {
        model.readAddresses(callBack: callBack)
    }

    func deleteAddress(index: Int, callBack: @escaping  (Bool) -> Void) {
        model.deleteAddress(index: index, callBack: callBack)
    }
}

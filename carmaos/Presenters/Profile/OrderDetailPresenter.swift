//
// Created by Eduardo Alvarez on 23/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class OrderDetailPresenter {

    private var model: OrderDetailModel
    var order: [String: Any] { return model.order }

    var statusCode: Int { return model.statusCode }
    var statusBarContent: [[String: Any]] { return model.statusBarContent }

    // MARK: - Constructor
    init(model: OrderDetailModel) {
        self.model = model
    }
    
    // MARK: - Format
    func showDate(date: Any) -> String {
        return StringHandler.formatDate(date: date, format: "EEEE dd MMMM, YYYY").capitalized
    }

    func formatItemUnits(quantity: Int) -> String {
        return StringHandler.formatUnit(quantity: quantity)
    }

    func formatItemTotal(price: Double, quantity: Int) -> String {
        let amount = model.getItemSubTotal(price: price, quantity: quantity)

        return StringHandler.formatPrice(amount: amount)
    }

    func formatPrice(amount: Double) -> String {
        return StringHandler.formatPrice(amount: amount)
    }
}

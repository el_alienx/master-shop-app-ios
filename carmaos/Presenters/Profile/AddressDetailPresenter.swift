//
// Created by Eduardo Alvarez on 23/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class AddressDetailPresenter {

    private var model: AddressDetailModel

    var address: Address? {
        get { return model.address }
        set { model.address = newValue }
    }

    // Constructor
    init(model: AddressDetailModel) {
        self.model = model
    }

    func setAddress(data: [String: Any], callBack: @escaping (Bool) -> Void) {
        model.setAddress(data: data, callBack: callBack)
    }
}
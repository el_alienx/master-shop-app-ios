//
//  FoodSubMenuPresenter.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 2/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import Firebase

final class SubMenuPresenter {
    
    private let model: SubMenuModel
    var collections: [[String: Any]] { return model.collections }
    var selectedDocument: Product {
        get { return model.selectedDocument!}
        set { model.selectedDocument = newValue }
    }
    var reference: CollectionReference {
        get { return model.reference}
        set { model.reference = newValue }
    }
    var menuCategories: [MenuCategory] { return model.menuCategories }

    // Constructor
    init(model: SubMenuModel) {
        self.model = model
    }
    
    func loadCollection(callback: @escaping () -> () ) {
        model.loadCollection(callback: callback )
    }

    // MARK: - Cart management
    func isItemInCart(productId: String) -> Bool {
        return model.isItemInCart(productId: productId)
    }

    func updateCartTabBadge() -> String? {
        return model.updateCartTabBadge()
    }
}

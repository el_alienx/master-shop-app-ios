//
//  DetailViewModel.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 29/7/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class ProductPresenter {

    private var model: ProductModel
    
    var product: Product { return model.product }

    var isInCart: Bool { return model.isInCart }
    var quantity: Int { return model.quantity }
    var quantityInCart: Int? { return model.quantityInCart }

    var isStoreOpen: Bool { return model.isStoreOpen }


    // MARK: - Constructor
    init(model: ProductModel) {
        self.model = model
    }

    // MARK: - Format methods
    func formatUnit(quantity: Int) -> String {
        return StringHandler.formatUnit(quantity: quantity)
    }
    
    func showSubTotal() -> String {
        let ammount = model.calculateSubTotal()
        
        if ammount == 0 {
            return ""
        }
        else {
            return StringHandler.formatPrice(amount: model.calculateSubTotal())
        }
    }

    // MARK: - Cart management
    func increaseUnits() {
        model.increaseUnits()
    }
    
    func decreaseUnits() {
        model.decreaseUnits()
    }
    
    func addItemToCart() {
        model.addItemToCart()
    }

    func updateItemFromCart() {
        model.updateItemFromCart()
    }

    func removeItemFromCart() {
        model.removeItemFromCart()
    }
}

//
//  FoodMenuPresenter.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 1/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class MenuPresenter {
    
    private let model: MenuModel
    
    var collection: [MenuCategory] { return model.collection }
    var selectedDocument: MenuCategory {
        get { return model.selectedDocument! }
        set { model.selectedDocument = newValue }
    }
    
    // Constructor
    init(model: MenuModel) {
        self.model = model
    }
}

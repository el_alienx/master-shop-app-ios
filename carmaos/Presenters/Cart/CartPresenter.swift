//
//  CartStartPresenter.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 30/7/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class CartPresenter {

    private var model: CartModel

    var cartItems: [CartItem] { get { return model.cartItems } }
    var isCartEmpty: Bool { return model.isCartEmpty }
    var isOrderPlaced: Bool { return model.isOrderPlaced }
    var isStoreClosed: Bool { return model.isStoreClosed }
    var isUserLogged: Bool { return model.isUserLogged }
    var addresses: [Address] { return model.addresses }
    
    // MARK: - Constructor
    init (model: CartModel) {
        self.model = model
    }

    // MARK: - Format methods
    func formatItemUnits(quantity: Int) -> String {
        return StringHandler.formatUnit(quantity: quantity)
    }
    
    func formatItemTotal(price: Double, quantity: Int) -> String {
        let amount = model.getItemSubTotal(price: price, quantity: quantity)
        
        return StringHandler.formatPrice(amount: amount)
    }
    
    func formatOrderTotalUnits() -> String {
        let count = cartItems.count
        let word = (count == 1) ? "ítem" : "ítems"
        
        return "Tienes \(String(count)) \(word) en el carrito"
    }
    
    func formatOrderSubtotal() -> String {
        let amount = model.getCartSubTotal()
        
        return StringHandler.formatPrice(amount: amount)
    }

    // MARK: - Cart management methods
    func removeItemFromCart(index: Int) {
        model.removeItemFromCart(index: index)
    }
    
    func updateCartTabBadge() -> String? {
        return model.updateCartTabBadge()
    }
}

//
//  CheckoutPresenter.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 14/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class CheckoutPresenter {
    
    private var model: CheckoutModel

    private var shippingCost: Double { return model.shippingCost }
    private var isCardPayment: Bool { return model.isCardPayment }
    var comments: String {
        get { return model.comments }
        set { model.comments = newValue }
    }
    var successStatus: Bool? {
        get { return model.successStatus }
        set { model.successStatus = newValue }
    }

    // MARK: - Constructor
    init(model: CheckoutModel) {
        self.model = model
    }
    
    // MARK: - Format text functions
    func showSubtotal() -> String {
        let amount = model.getOrderSubTotal()
        return StringHandler.formatPrice(amount: amount )
    }
    
    func showShipping() -> String {
        var result = "Gratis"
        
        if shippingCost > 0 {
            result = StringHandler.formatPrice(amount: shippingCost )
        }
        
        return result
    }
    
    func showTransactionCost() -> String? {
        var result: String?
        
        if isCardPayment {
            let amount = model.getOrderTransactionalCost()
            result = StringHandler.formatPrice(amount: amount)
        }
        
        return result
    }
    
    func showTotal() -> String {
        let amount = model.getOrderTotal()
        return StringHandler.formatPrice(amount: amount)
    }
    
    // MARK: - Place order
    func placeOrder(callBack: @escaping (Bool) -> () ) {
        model.placeOrder(callBack: callBack )
    }
    
    func clearCart() {
        model.clearCart()
    }
}

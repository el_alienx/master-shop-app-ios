//
// Created by Eduardo Alvarez on 16/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class CheckoutResultPresenter {

    private let model: CheckoutResultModel
    var stateSuccess: StateMessage { return model.stateSuccess }
    var stateFail: StateMessage { return model.stateFail }

    // Constructor
    init(model: CheckoutResultModel) {
        self.model = model
    }

    func completeOrder() {
        model.completeOrder()
    }

    func updateCartBadge() -> String? {
        return model.updateCartBadge()
    }
}
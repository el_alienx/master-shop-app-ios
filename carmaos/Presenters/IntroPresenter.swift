//
//  OmboardingPresenter.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 3/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class IntroPresenter {
    
    private let model: IntroModel
    var collection: [MenuCategory] { return model.collection }
    var isReturning: Bool {
        get { return model.isReturning }
        set { model.isReturning = newValue }
    }
    
    // Constructor
    init (model: IntroModel) {
        self.model = model
    }
}

//
//  CardBig.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 25/7/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class CardBig: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func setupComponent() {
        self.contentView.layer.cornerRadius = 8
        self.contentView.layer.masksToBounds = true
        
        self.layer.shadowColor = UIColor.black.cgColor // Sketch Color
        self.layer.shadowOpacity = 0.266 // Sketch Alpha 0.266
        self.layer.shadowOffset = CGSize(width: 0, height: 1.333) // Sketch X, Y
        self.layer.shadowRadius = 3.333 / 2.0 // Any number / 2 to obtain the Sketch blur look
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds,
                                             cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
}

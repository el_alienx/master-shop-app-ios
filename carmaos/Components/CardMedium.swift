//
//  CardSmall.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 26/7/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class CardMedium: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var check: UIImageView!
    @IBOutlet weak var checkCircle: UIImageView!
}

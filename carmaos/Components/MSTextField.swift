//
//  InputField.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 19/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class MSTextField: UIView {
    
    var maxLength: Int?
    var text: String? {
        get { return getText() }
        set { setText(value: newValue) }
    }
    var errorMessage: String?

    // Validations
    var required = false
    var requiredLength = false
    var requiredName = false
    var requiredNumber = false
    var requiredPhoneBasic = false

    
    @IBOutlet weak private var contentView: UIView!
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak internal var errorLabel: UILabel!
    @IBOutlet weak private var textField: UITextField!
    @IBOutlet weak private var border: UIView!
    
    // for using custom view in code
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    // for using custom view in IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize()
    }
    
    private func initialize() {
        Bundle.main.loadNibNamed("MSTextField", owner: self, options: nil)
        
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        errorLabel.text = errorMessage
        textField.delegate = self
    }
    
    // Constructor
    func start(title: String, placeholder: String, maxLength: Int)  {
        titleLabel.text = title
        textField.placeholder = placeholder
        
        self.maxLength = maxLength
    }

    func setKeyboardType (keyboardType: UIKeyboardType) {
        textField.keyboardType = keyboardType
    }

    func getText() -> String? {
        return textField.text
    }

    func setText(value: String?) {
        textField.text = value
    }
}

extension MSTextField: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        titleLabel.textColor = Brand.color_dominant
        border.backgroundColor = Brand.color_dominant
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let lenght = maxLength else { return true }
        guard let text = textField.text else { return true }
        
        let newLength = text.count + string.count - range.length
        return newLength <= lenght
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if validate() {
            print("Field approved")
        }
    }
}

extension MSTextField {

    func validate() -> Bool {
        var result = true

        // Inverse If's chain
        // The important items are at the bottom
        if requiredName && !validateName() { result = false }
        if requiredPhoneBasic && !validatePhoneBasic() { result = false }
        if requiredLength && !validateLength() { result = false }
        if requiredNumber && !validateNumber() { result = false }
        if !validateEmpty() { result = false }

        // Refactor move elsewhere
        titleLabel.textColor = result ? Color.inputTitle : Color.isDanger
        border.backgroundColor = result ? Color.inputBorder : Color.isDanger
        errorLabel.text = result ? nil : errorMessage
        
        return result
    }

    private func validateEmpty() -> Bool {
        let result = (textField.text?.count)! > 0

        if !result { errorMessage = "Este dato es necesario" }
        return result
    }

    private func validateLength() -> Bool {
        guard let length = maxLength else {
            return true
        }
        
        let result = textField.text?.count == length
        
        if !result { errorMessage = "Debe tener \(length) números" }
        return result
    }

    private func validateName() -> Bool {
        let string = textField.text!
        let wordCount = string.trimmingCharacters(in: .whitespaces).components(separatedBy: .whitespaces).filter {$0 != ""}.count

        switch wordCount {
        case 0:
            errorMessage = "Debe contener nombre y apellido"
            return false
        case 1:
            errorMessage = "Falta el apellido"
            return false
        default:
            return true
        }
    }

    private func validateNumber() -> Bool {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: textField.text!)
        let result = allowedCharacters.isSuperset(of: characterSet)

        if !result { errorMessage = "Solo poner números" }
        return result
    }

    private func validatePhoneBasic() -> Bool {
        if !self.validateEmpty() {
            return false
        }

        let phone = textField.text!
        let index = phone.index(phone.startIndex, offsetBy: 0)
        let result = String(phone[index]) == "0"
        
        if !result { errorMessage = "El celular debe empezar con 0" }
        return result
    }
}

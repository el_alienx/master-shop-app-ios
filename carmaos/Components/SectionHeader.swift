//
//  SectionHeader.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 27/7/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class SectionHeader: UICollectionReusableView {
    @IBOutlet weak var title: UILabel!
}
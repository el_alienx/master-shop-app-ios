//
//  TextAreaField.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 1/9/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class MSTextView: UIView {

    var placeholder: String?
    var maxLength: Int?
    var text: String? {
        get { return getText() }
        set { setText(value: newValue) }
    }
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var placeholderTextView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var leftBorder: UIView!
    @IBOutlet weak var rightBorder: UIView!
    @IBOutlet weak var bottomBorder: UIView!
    
    
    // for using custom view in code
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    // for using custom view in IB
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialize()
    }
    
    private func initialize() {
        Bundle.main.loadNibNamed("MSTextView", owner: self, options: nil)
        
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        textView.delegate = self
    }
    
    // Constructor
    func start(title: String, placeholder: String ,maxLength: Int)  {
        self.placeholder = placeholder
        self.maxLength = maxLength
        
        titleLabel.text = title
        placeholderTextView.text = placeholder
        placeholderTextView.textColor = Color.inputPlaceholder
    }
    
    func setKeyboardType (keyboardType: UIKeyboardType) {
        textView.keyboardType = keyboardType
    }
    
    func getText() -> String? {
        return textView.text
    }
    
    func setText(value: String?) {
        textView.text = value
        placeholderTextView.text = ""
    }
    
    internal func changeBorderColor(color: UIColor) {
        leftBorder.backgroundColor = color
        rightBorder.backgroundColor = color
        bottomBorder.backgroundColor = color
    }
}

extension MSTextView: UITextViewDelegate {

    func textViewDidBeginEditing(_ textView: UITextView) {
        titleLabel.textColor = Brand.color_dominant
        changeBorderColor(color: Brand.color_dominant)

        placeholderTextView.text = ""
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let length = maxLength else { return true }
        guard let textViewText = textView.text else { return true }
        
        let newLength = textViewText.count + text.count - range.length
        return newLength <= length
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        titleLabel.textColor = Color.inputTitle
        changeBorderColor(color: Color.inputBorder)
        
        if textView.text.isEmpty {
            placeholderTextView.text = placeholder
        }
    }
}

//
//  CardMini.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 4/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class CardSmall: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
}

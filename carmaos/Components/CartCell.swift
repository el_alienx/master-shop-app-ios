//
//  CartCell.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 30/7/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

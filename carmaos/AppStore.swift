//
//  AppStore.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 24/7/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import UIKit

let menuNotificationKey = "com.mastershop.menu"
let restaurantNotificationKey = "com.mastershop.restaurant"

final class AppStore {

    public static var menu: Menu!
    public static var shoppingCart: ShoppingCart!
    public static var restaurant: Restaurant!
    public static var user: User!
    public static var notificationDialog: NotificationDialog!
    
    private static var debug = false
    
    // Custom constructor
    static func start(application: UIApplication) {
        let menuReference = FirebaseHandler.getStoreReference().collection("menu")
        let restaurantReference = FirebaseHandler.getStoreReference()
            
        menu = Menu(reference: menuReference)
        restaurant = Restaurant(reference: restaurantReference)
        shoppingCart = ShoppingCart()
        user = User()
        notificationDialog = NotificationDialog(application: application)
        
        if debug { setDebugOptions() }
    }
    
    private static func setDebugOptions() {
        user.name = "Eduardo Xavier"
        user.phone = "0999065568"
        user.addresses.append( Address(data: [
            "urbanization": "Garzota",
            "street": "Avenida Guillermo Rolando Pareja",
            "numeration": "Mz 97. Solar 3.",
            "reference": "Atrás del Banco Pichincha. Edificio gris con ventanas azules"
        ]))
    }
}

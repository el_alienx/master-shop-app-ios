//
//  Address.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 15/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

struct Address: Encodable {

    var id: String?
    var urbanization: String
    var street: String
    var numeration: String
    var reference: String?
    var name: String?
    
    static let urbTitle = "Cdla. o Urbanización"
    static let stTitle = "Calles"
    static let numTitle = "Manzana y Villa"
    static let refTitle = "Referencia (opcional)"
    
    static let urbPlaceholder = "Las Garzas"
    static let stPlaceholder = "Av. 9 de Octubre y Hurtado."
    static let numPlaceholder = "Mz 1. Villa 44."
    static let refPlaceholder = "Edificio azul con ventanas azules.\nAtrás del banco Pichincha."
    
    init(data: [String: Any]) {
        self.id = data["id"] as? String
        self.urbanization = data["urbanization"] as! String
        self.street = data["street"] as! String
        self.numeration = data["numeration"] as! String
        self.reference = data["reference"] as? String ?? "referencia por default"
        self.name = data["name"] as? String ?? "referencia por default"
    }
}

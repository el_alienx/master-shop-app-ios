//
// Created by Eduardo Alvarez on 16/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

struct StateMessage {

    var image: String!
    var title: String!
    var description: String!
    var buttonTitle: String!

    init(image: String, title: String, description: String, buttonTitle: String) {
        self.image = image
        self.title = title
        self.description = description
        self.buttonTitle = buttonTitle
    }
}
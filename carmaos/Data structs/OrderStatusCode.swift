//
// Created by Eduardo Alvarez on 31/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

enum OrderStatusCode: String {
    case Cancel = "Cancelado"
    case Open = "Abierta"
    case Progress = "En proceso"
    case Transit = "En tránsito"
    case Delivered = "Entregada"
}
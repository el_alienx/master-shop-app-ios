//
//  Product.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 15/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

struct Product {
    
    var id: String
    var name: String
    var price: Double
    var description: String?
    var image: String?
    var thumbnail: String?
    
    init(data: [String: Any]) {
        self.id = data["id"] as! String
        self.name = data["title"] as! String
        self.price = data["price"] as! Double
        self.description = data["text"] as? String ?? ""
        self.image = data["image"] as? String ?? ""
        self.thumbnail = data["image_thumbnail"] as? String ?? ""
    }
}

//
//  CartItem.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 15/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

struct CartItem: Codable {
    
    var id: String
    var name: String
    var price: Double
    var quantity: Int
    var description: String?
    var image: String?
    
    init(data: [String: Any]) {
        self.id = data["id"] as! String
        self.name = data["title"] as! String
        self.price = data["price"] as! Double
        self.quantity = data["quantity"] as! Int
        self.description = data["text"] as? String ?? ""
        self.image = data["image"] as? String ?? ""
    }
}

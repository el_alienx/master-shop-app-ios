//
//  SubMenuHeader.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 15/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

struct SubMenuHeader {
    
    var id: String
    var title: String
    
    init(data: [String: Any]) {
        self.id = data["id"] as! String
        self.title = data["title"] as? String ?? ""
    }
}

//
//  MenuCategory.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 15/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

struct MenuCategory {
    
    var id: String
    var index: Int
    var title: String
    var subtitle: String
    var image: String
    var thumbnail: String
    
    init(data: [String: Any]) {
        self.id = data["id"] as! String
        self.index = data["index"] as! Int
        self.title = data["title"] as! String
        self.subtitle = data["subtitle"] as? String ?? ""
        self.image = data["image"] as? String ?? ""
        self.thumbnail = data["image_thumbnail"] as? String ?? ""
    }
}

//
//  CostsItem.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 15/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

struct CostsItem {
    var title: String
    var detail: String
    
    init (title: String, detail: String) {
        self.title = title
        self.detail = detail
    }
}

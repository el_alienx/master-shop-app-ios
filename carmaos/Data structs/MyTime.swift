//
// Created by Eduardo Alvarez on 17/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

struct MyTime {
    var hour: Int!
    var minute: Int!

    init(hour: Int, minute: Int) {
        self.hour = hour
        self.minute = minute
    }
}
//
//  CheckoutModel.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 14/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import CodableFirebase

final class CheckoutModel {
    
    private var shoppingCart: ShoppingCart
    private var restaurant: Restaurant
    private var user: User

    private let transactionalPercentage: Double!
    private let reference = FirebaseHandler.getStoreReference().collection("orders")
    var isCardPayment: Bool!
    let shippingCost: Double!
    var successStatus: Bool?
    var comments: String = "No hay indicaciones"

    // MARK: - Constructor
    init(shoppingCart: ShoppingCart, restaurant: Restaurant, user: User) {
        self.shoppingCart = shoppingCart
        self.restaurant = restaurant
        self.user = user

        self.isCardPayment = user.selectedPayment == "Datafast"
        self.shippingCost = restaurant.shippingCost
        self.transactionalPercentage = restaurant.transactionalPercentage
    }

    func getAddress() -> [String: Any] {
        var result: [String: Any] = [:]

        if user.isLogged && user.selectedAddress != nil {
            result = try! FirestoreEncoder().encode(user.addresses[user.selectedAddress!])
        }
        else {
            result = user.tempAddress!
        }

        print("Checkout model getAddress result = \(result)")

        return result
    }

    // MARK: - Calculate costs
    func getOrderSubTotal() -> Double {
        return shoppingCart.getSubtotal()
    }

    func getOrderTransactionalCost() -> Double {
        var result: Double = 0

        if isCardPayment {
            let amount = shoppingCart.getSubtotal() + shippingCost
            result = amount * transactionalPercentage
        }
        
        return result
    }
    
    func getOrderTotal() -> Double {
        var result = shoppingCart.getSubtotal() + shippingCost
        
        if isCardPayment {
            result = result + (result * transactionalPercentage)
        }
        
        return result
    }
    
    // MARK: - Place order
    func placeOrder(callBack: @escaping (Bool) -> () ) {
        let newOrder = createOrderItem()
        
        FirebaseHandler.createNewDocument(reference: reference, data: newOrder, completion: { success, id in
            callBack(success)
            self.storeUserOrder(orderId: id)
        })
    }
    
    func createOrderItem() -> [String: Any] {
        let codableCart = try! FirebaseEncoder().encode(shoppingCart.cartItems)
        let orderItem = [
            "address": getAddress(),
            "comments": comments,
            "date": Date(),
            "name": user.name!,
            "notificationToken": user.notificationToken ?? "no token available",
            "payment_method": user.selectedPayment!,
            "phone": user.phone!,
            "shipping_cost": shippingCost,
            "shopping_cart": codableCart,
            "status": 0,
            "statusMessage": "",
            "store_name": Brand.store,
            "sub_total": getOrderSubTotal(),
            "total_price": getOrderTotal(),
            "transactional_cost": getOrderTransactionalCost(),
            "uid": user.uid!
        ]
        
        return orderItem
    }

    // MARK: - Cart management    
    func clearCart() {
        shoppingCart.clearCart()
    }

    // MARK - User management
    func storeUserOrder(orderId: String) {
        let query = user.reference.document(user.uid!).collection("orders").document(Brand.store).collection("orders")
        let newOrder = createOrderItem()
        
        func callBack(success: Bool) {
            if !success {
                print("CheckoutModel.swift - Could not store the order in the user account")
                return
            }
        }

        FirebaseHandler.createDocumentWithId(reference: query, id: orderId, data: newOrder, completion: callBack)
    }
}

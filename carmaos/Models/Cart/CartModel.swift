//
//  CartStartModel.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 30/7/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class CartModel {

    private var shoppingCart: ShoppingCart
    private var restaurant: Restaurant
    private var user: User

    var cartItems: [CartItem] { return shoppingCart.cartItems }
    var isCartEmpty: Bool { return checkCartIsEmpty() }
    var isStoreClosed: Bool { return !restaurant.isStoreOpen}
    var isOrderPlaced: Bool { return user.isOrderPlaced }
    var isUserLogged: Bool { return user.isLogged }
    var addresses: [Address] { return user.addresses }

    // MARK: - Constructor
    init (shoppingCart: ShoppingCart, restaurant: Restaurant, user: User) {
        self.shoppingCart = shoppingCart
        self.restaurant = restaurant
        self.user = user
    }

    // MARK: - Cart management methods
    func removeItemFromCart(index: Int) {
        shoppingCart.removeItemFromCartByIndex(at: index)
    }

    func updateCartTabBadge() -> String? {
        return shoppingCart.updateCartBadge()
    }

    func checkCartIsEmpty() -> Bool {
        return shoppingCart.cartItems.count == 0
    }

    // MARK: - Item management methods
    func getItemSubTotal(price: Double, quantity: Int) -> Double {
        return price * Double(quantity)
    }

    func getCartSubTotal() -> Double {
        return shoppingCart.getSubtotal()
    }
}

//
// Created by Eduardo Alvarez on 16/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class CheckoutResultModel {

    private var shoppingCart: ShoppingCart
    private var user: User

    let stateSuccess = StateMessage(
        image: "glyph-star",
        title: "¡Tu primer pedido ha sido un exito!",
        description: "En la sección de perfil podrás ver el como va el proceso de envío.",
        buttonTitle: "Ver Orden"
    )
    let stateFail = StateMessage(
        image: "glyph-internet",
        title: "¡Se fue el internet!",
        description: "Revisa que tengas internet por WIFI o plan de datos.",
        buttonTitle: "Regresar y Probar de Nuevo"
    )

    // Constructor
    init(shoppingCart: ShoppingCart, user: User) {
        self.shoppingCart = shoppingCart
        self.user = user
    }

    func completeOrder() {
        user.isOrderPlaced = true
        shoppingCart.clearCart()
    }

    func updateCartBadge() -> String? {
        return shoppingCart.updateCartBadge()
    }
}
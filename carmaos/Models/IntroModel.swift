//
//  OnboardingModel.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 3/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class IntroModel {
    
    private let menu: Menu
    private let user: User
    var collection: [MenuCategory] { return menu.menuCategories }
    var isReturning: Bool {
        get { return user.isReturning }
        set { user.setIsReturning() }
    }
    
    // Constructor
    init(menu: Menu, user: User) {
        self.menu = menu
        self.user = user
    }
}

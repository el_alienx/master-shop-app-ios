//
//  ProfileModel.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 21/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class ProfileModel {
    
    private var user: User!

    var isLogged: Bool { return user.isLogged }
    var isOrderPlaced: Bool { return user.isOrderPlaced }
    var latestOrder: [String: Any]? { return user.latestOrder }
    var name: String? { return user.name }

    let authSegue: String = "showAuth"
    let menuOptions: [[String: String]] = [
        ["label": "Direcciones de envío", "icon": "icon-address-book", "segue": "showAddressBook"],
        ["label": "Historial de pedidos", "icon": "icon-order-history", "segue": "showOrderHistory"]
    ]

    // Constructor
    init(user: User) {
        self.user = user
    }

    func signOut() {
        user.signOut()
    }

    func readLatestOrder(callBack: @escaping (Bool) -> Void) {
        user.readLatestOrder(callBack: callBack)
    }
}

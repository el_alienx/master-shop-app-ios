//
// Created by Eduardo Alvarez on 23/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class AddressDetailModel {

    private var user: User

    var address: Address?

    // Constructor
    init(user: User) {
        self.user = user
    }

    func setAddress(data: [String: Any], callBack: @escaping (Bool) -> Void) {
        guard let id: String = data["id"] as? String else {
            user.createAddress(data: data, callBack: callBack)
            return
        }

        user.updateAddress(id: id, data: data, callBack: callBack)
    }
}

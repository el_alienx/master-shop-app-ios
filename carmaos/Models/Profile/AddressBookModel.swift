//
// Created by Eduardo Alvarez on 23/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class AddressBookModel {

    private var user: User

    let segue: String = "showAddressDetail"
    var addresses: [Address] { return user.addresses }

    // Constructor
    init(user: User) {
        self.user = user        
    }

    func readAddresses(callBack: @escaping  (Bool) -> Void) {
        guard let uid = user.uid else {
            print("AddressBookModel.swift fetchAddressBook() No uid is available.")
            return
        }

        user.readAddresses(uid: uid, callBack: callBack)
    }

    func deleteAddress(index: Int, callBack: @escaping (Bool) -> Void) {
        user.deleteAddress(index: index, callBack: callBack)
    }
}

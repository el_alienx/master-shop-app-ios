//
// Created by Eduardo Alvarez on 23/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class OrderHistoryModel {

    private var user: User
    
    var orders: [[String: Any]] { return user.orders }

    // Constructor
    init(user: User) {
        self.user = user
    }

    func readOrders(callBack: @escaping () -> Void) {
        user.readOrders(callBack: callBack)
    }
}

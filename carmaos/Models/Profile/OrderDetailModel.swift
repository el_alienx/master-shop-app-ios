//
// Created by Eduardo Alvarez on 23/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class OrderDetailModel {

    private var user: User
    var order: [String: Any]

    let statusCode: Int
    let statusBarContent: [[String: Any]] = [
        ["text": "Orden realizada", "icon": "icon-bell", "color": Brand.color_dominant],
        ["text": "Orden leída", "icon": "icon-bell", "color": Brand.color_dominant],
        ["text": "Preparando tu pedido", "icon": "icon-hourglass", "color": Color.isInfo],
        ["text": "Pedido entregado", "icon": "icon-check-line", "color": Color.isSuccess],
        ["text": "Pedido cancelado", "icon": "icon-sad", "color": Color.isDanger]
    ]

    // Constructor
    init(user: User, order: [String: Any]) {
        self.user = user
        self.order = order

        statusCode = order["status"] as! Int
    }

    func getItemSubTotal(price: Double, quantity: Int) -> Double {
        return price * Double(quantity)
    }
}

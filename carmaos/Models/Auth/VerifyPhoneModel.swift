//
// Created by Eduardo Alvarez on 21/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class VerifyCodeModel {

    private let user: User!

    let maxLength: Int = 6
    var verificationID: String? { return user.verificationID }
    var uid: String? {
        get { return user.uid }
        set { user.uid = newValue }
    }
    var isNewUser: Bool {
        get { return user.isNewUser }
        set { user.isNewUser = newValue }
    }

    // Constructor
    init(user: User) {
        self.user = user
    }

    func confirmVerificationSMS(verificationID: String, verificationCode: String, callBack: @escaping (Bool, String, Bool) -> Void) {
        FirebaseHandler.confirmVerificationSMS(
            verificationID: verificationID,
            verificationCode: verificationCode,
            completion: { success, uid, isNewUser in
                callBack(success, uid, isNewUser)
            }
        )
    }
}
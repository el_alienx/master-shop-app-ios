//
// Created by Eduardo Alvarez on 21/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class AuthModel {

    private let user: User

    let maxLength = 10
    var name: String? {
        get { return user.name }
        set { user.name = newValue }
    }
    var phone: String? {
        get { return user.phone }
        set { user.phone = newValue }
    }
    var verificationID: String? {
        get { return user.verificationID }
        set { user.verificationID = newValue }
    }

    // Constructor
    init(user: User) {
        self.user = user
    }

    func sendVerificationSMS(phone: String, callBack: @escaping (String) -> () ) {
        FirebaseHandler.sendVerificationSMS(phoneNumber: phone, completion: { uid in
            callBack(uid)
        })
    }
}
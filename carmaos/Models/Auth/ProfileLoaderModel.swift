//
// Created by Eduardo Alvarez on 22/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class AuthCompleteModel {

    private var user: User

    var onAuthCompleteSegue: String? { return user.onAuthCompleteSegue }

    // Constructor
    init(user: User) {
        self.user = user
    }

    func loadProfile(callBack: @escaping (Bool) -> Void) {
        guard let uid = user.uid else {
            print("ProfileLoaderModel.swift - No uid available")
            return
        }

        if user.isNewUser {
            guard let name = user.name else {
                return
            }

            guard let phone = user.phone else {
                return
            }

            user.createNewUser(uid: uid, name: name, phone: phone, callBack: callBack)
        } else {
            user.signIn(uid: uid, callBack: callBack)
        }
    }
}
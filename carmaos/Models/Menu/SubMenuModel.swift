//
//  FoodSubMenuModel.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 2/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import Firebase

final class SubMenuModel {
    
    private var menu = AppStore.menu! // Refactor: This should be pass as reference
    private var shoppingCart = AppStore.shoppingCart! // Refactor: This should be pass as reference
    var menuCategories: [MenuCategory] { return menu.menuCategories }
    var collections: [[String: Any]] = []
    var myCallBack: ( () -> () )?
    var selectedDocument: Product?
    let receivedDocument: MenuCategory
    var reference: CollectionReference

    // MARK: - Constructor
    init(receivedDocument: MenuCategory, reference: CollectionReference) {
        self.receivedDocument = receivedDocument
        self.reference = reference
    }

    // MARK: - Menu management
    func loadCollection(callback: @escaping () -> ()) {
        reference.order(by: "index").getDocuments(){ (querySnapshot, err) in
            if let err = err {
                print("SubMenu.swift - Error getting documents: \(err)")
            } else {
                self.collections = FirebaseHandler.parseCollection(querySnapshot: querySnapshot!)

                self.loopSubCollection()
                self.myCallBack = callback
            }
        }
    }
    
    func loopSubCollection() {
        for (index, _) in collections.enumerated() {
            let myId = collections[index]["id"] as! String
            
            loadSubCollection(id: myId, index: index)
        }
    }
    
    func loadSubCollection(id: String, index: Int)  {
        reference.document(id).collection("subcollection").order(by: "index").getDocuments(){ (querySnapshot, err) in
            if let err = err {
                print("SubMenu.swift - Error getting documents: \(err)")
            } else {
                self.collections[index]["collection"] = FirebaseHandler.parseCollection(querySnapshot: querySnapshot!)
               
                // now we call he callback
                self.myCallBack!()
            }
        }
    }

    // Mark: ShoppingCart management
    func isItemInCart(productId: String) -> Bool {
        return shoppingCart.isItemInCart(productId: productId)
    }

    func updateCartTabBadge() -> String? {
        return shoppingCart.updateCartBadge()
    }
}

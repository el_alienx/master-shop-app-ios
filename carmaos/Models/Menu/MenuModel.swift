//
//  FoodMenuModel.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 1/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class MenuModel {
    
    private let menu: Menu
    var collection: [MenuCategory] { return menu.menuCategories }
    var selectedDocument: MenuCategory?
    
    // Constructor
    init(menu: Menu) {
        self.menu = menu
    }
}

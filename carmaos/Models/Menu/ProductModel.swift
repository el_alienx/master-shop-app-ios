//
//  DetailModel.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 29/7/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class ProductModel {

    var product: Product
    private var shoppingCart: ShoppingCart
    private var restaurant: Restaurant
    
    // Cart management
    var isInCart: Bool
    var quantity: Int
    let quantityInCart: Int?
    private let minQuantity: Int
    private let maxQuantity: Int = 10

    // Restaurant management
    var isStoreOpen: Bool { return restaurant.isStoreOpen }


    // MARK: - Constructor
    init(product: Product, shoppingCart: ShoppingCart, restaurant: Restaurant) {
        self.product = product
        self.shoppingCart = shoppingCart
        self.restaurant = restaurant
        
        self.isInCart = shoppingCart.isItemInCart(productId: product.id)
        self.quantity = shoppingCart.getItemQuantity(productId: product.id) ?? 1
        self.quantityInCart = self.isInCart ? quantity : nil
        self.minQuantity = self.isInCart ? 0 : 1
    }

    // MARK: - Cart management
    func increaseUnits() {
        if (quantity < maxQuantity) {
            quantity += 1
            SoundPlayer.playSound(wavFile: "tap")
        }
    }

    func decreaseUnits() {
        if (quantity > minQuantity) {
            quantity -= 1
            SoundPlayer.playSound(wavFile: "tap")
        }
    }

    func addItemToCart() {
        let item = CartItem(data: [
            "id": product.id,
            "title": product.name,
            "price": product.price,
            "quantity": quantity,
            "text": product.description ?? "",
            "image": product.image ?? "",
            "thumbnail": product.thumbnail ?? ""
        ])

        shoppingCart.addItemToCart(newItem: item)
    }

    func updateItemFromCart() {
        shoppingCart.updateItemFromCart(id: product.id, quantity: quantity)
    }

    func removeItemFromCart() {
        shoppingCart.removeItemFromCartById(id: product.id)
    }

    // Refactor suggestion: Move to a "Calculator" class?
    func calculateSubTotal() -> Double {
        return product.price * Double(quantity)
    }
}

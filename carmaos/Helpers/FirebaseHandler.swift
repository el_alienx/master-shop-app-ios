//
//  FirebaseHandler.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 15/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import Firebase

final class FirebaseHandler {
    private static var store: String!
    private static var firebase = Firestore.firestore()

    // MARK: - Constructor
    static func start (store: String) {
        self.store = store

        FirebaseConfiguration.shared.setLoggerLevel(FirebaseLoggerLevel.min)
        FirebaseApp.configure()
        Firestore.firestore().settings = FirestoreSettings()
    }
    
    // MARK: - Route references
    static func getRootReference () -> Firestore {
        return firebase
    }

    static func getStoreReference () -> DocumentReference {
        return firebase.collection("stores").document(store)
    }
}
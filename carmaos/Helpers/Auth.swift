//
// Created by Eduardo Alvarez on 27/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import FirebaseAuth

extension FirebaseHandler {
    static func sendVerificationSMS(phoneNumber: String?, completion: @escaping (String) -> () ) {
        // Functional

        guard let phoneNumber: String = phoneNumber else {
            print("Auth.swift - There is not a phone number")
            return
        }

        let fullPhone = "+593\(phoneNumber)"

        Auth.auth().languageCode = "es"
        PhoneAuthProvider.provider().verifyPhoneNumber(fullPhone, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print("Auth.swift - \(error.localizedDescription)");
                return
            }

            guard let verification = verificationID else {
                print("Auth.swift - There is not a verification ID available")
                return
            }

            completion(verification)
        }
    }

    static func confirmVerificationSMS(verificationID: String, verificationCode: String, completion: @escaping (Bool, String, Bool) -> Void) {
        // Functional

        let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: verificationID,
                verificationCode: verificationCode)

        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                print("Auth.swift - Error retrieving data \(error)")
                completion(false, "", false)
                return
            }

            guard let user = Auth.auth().currentUser else {
                print("Auth.swift - Current user uid is not available")
                completion(false, "", false)
                return
            }

            guard let additionalUserInfo = authResult!.additionalUserInfo else {
                print("Auth.swift - Info required for checking if new user is not available")
                completion(false, "", false)
                return
            }

            let uid: String = user.uid
            let isNewUser: Bool = additionalUserInfo.isNewUser

            completion(true, uid, isNewUser)
        }
    }
}
//
//  NotificationHandler.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 27/9/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import UserNotifications
import FirebaseMessaging
import FirebaseInstanceID

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func setNotificationDelegates() {        
        // iOS delegate
        UNUserNotificationCenter.current().delegate = self
        
        // Firebase delegate
        Messaging.messaging().delegate = self
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        print(userInfo)
        
        completionHandler()
    }
}

//
//  KeyboardHandler.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 11/9/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift

final class KeyboardHandler {
    
    // MARK: - Constructor
    static func start() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    }
}

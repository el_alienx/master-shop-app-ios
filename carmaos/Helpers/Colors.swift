//
// Created by Eduardo Alvarez on 2/9/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import UIKit

class Color {

    // Status
    static let isDanger = UIColor(red: 255/255, green: 45/255, blue: 85/255, alpha: 1)
    static let isSuccess = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
    static let isInfo = UIColor(red: 90/255, green: 200/255, blue: 250/255, alpha: 1)

    // TextField and TextView
    static let inputTitle = UIColor(red: 142/255, green: 142/255, blue: 147/255, alpha: 1)
    static let inputBorder = UIColor(red: 188/255, green: 187/255, blue: 193/255, alpha: 1)
    static let inputPlaceholder = UIColor(red: 204/255, green: 204/255, blue: 216/255, alpha: 1)
    
    // Other colors
    static let white = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
}

//
//  File.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 3/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class StringHandler {

    // Mark: Format
    static func formatUnit(quantity: Int, singular: String = "unidad", plural: String = "unidades") -> String {
        let adjective = (quantity == 1) ? singular : plural
        
        return "\(String(quantity)) \(adjective)"
    }
    
    static func formatPrice(amount: Double) -> String {
        return "$\(String(format:"%.02f", amount))"
    }
    
    static func formatDate(date: Any, format: String) -> String {
        let dateFormat = DateFormatter()
        dateFormat.locale = NSLocale(localeIdentifier: "es") as Locale?
        dateFormat.dateFormat = format
        
        return dateFormat.string(from: date as! Date)
    }
}

//
// Created by Eduardo Alvarez on 27/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import Firebase

extension FirebaseHandler {
    // MARK: - Create
    static func createNewDocument(reference: CollectionReference, data: [String: Any], completion: @escaping (Bool, String) -> ()) {
        var ref: DocumentReference? = nil
        ref = reference.addDocument(data: data) { error in
            if let error = error {
                print("FireStore.swift - Error creating document: \(error)")
                completion(false, "nil")
                return
            }

            completion(true, ref!.documentID)
        }
    }

    static func createDocumentWithId(reference: CollectionReference, id: String, data: [String: Any], completion: @escaping (Bool) -> ()) {
        reference.document(id).setData(data) { error in
            if let error = error {
                print("FireStore.swift - Error creating document: \(error) width id: \(id)")
                completion(false)
                return
            }

            completion(true)
        }
    }

    // MARK: - Read
    static func readDocument(reference: DocumentReference, completion: @escaping ([String: Any]) -> ()) {
        reference.getDocument { (document, error) in
            if let error = error {
                print("FireStore.swift - Error trying to read document: \(error)")
                return
            }

            guard let data = document?.data() else {
                print("FireStore.swift - Document don't exist")
                return
            }

            completion(data)
        }
    }

    static func readCollection(query: Query, completion: @escaping ([[String: Any]]) -> Void ) {
        query.getDocuments() { (querySnapshot, error) in
            if let error = error {
                print("FireStore.swift - Error trying to read collection: \(error)")
                return
            }

            let data: [[String: Any]] = parseCollection(querySnapshot: querySnapshot!)
            completion(data)
        }
    }

    // MARK - Update
    static func updateDocument(reference: CollectionReference, id: String, data: [String: Any], completion: @escaping (Bool) -> Void) {
        reference.document(id).setData(data, merge: true) { error in
            if let error = error {
                print("FireStore.swift - Error writing document: \(error)")
                completion(false)
                return
            }

            completion(true)
        }
    }

    // MARK - Delete
    static func deleteDocument(reference: CollectionReference, id: String, completion: @escaping (Bool) -> Void) {
        reference.document(id).delete() { error in
            if let error = error {
                print("FireStore.swift - Error removing document: \(error)")
                completion(false)
                return
            }
            completion(true)
        }
    }

    // MARK - Helper
    static func parseCollection(querySnapshot: QuerySnapshot) -> [[String: Any]] {
        var collection: [[String: Any]] = []

        for document in querySnapshot.documents {
            var doc:[String: Any] = document.data()
            doc["id"] = document.documentID

            collection.append(doc)
        }

        return collection
    }
}
//
// Created by Eduardo Alvarez on 16/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import AVFoundation

final class SoundPlayer {
    private static var player: AVAudioPlayer?

    static func playSound(wavFile: String) {
        guard let url = Bundle.main.url(forResource: wavFile, withExtension: "wav")
                else {
            return
        }

        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)

            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            guard let player = player else { return }

            player.play()

        } catch let error {
            print("SoundPlayer.swift \(error.localizedDescription)")
        }
    }
}
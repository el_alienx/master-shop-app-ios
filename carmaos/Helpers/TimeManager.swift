//
// Created by Eduardo Alvarez on 16/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class DateTimeHandler {
    static let currentTime = extractCurrentTime()

    // MARK - Public methods
    static func checkIfTodayIsWeekday() -> Bool {
        var result = false
        let currentDay = extractCurrentDayNumber()

        // 1 is Sunday, the day of our Lord you heretics!
        // 7 is Saturday, because of reasons
        if (currentDay != 1 && currentDay != 7) {
            result = true
        }

        return result
    }

    static func checkIfTimeRange(schedule: [String]) -> Bool {
        let start = schedule[0]
        let end = schedule[1]

        return checkStartRange(string: start) && checkEndRange(string: end)
    }

    static func checkIfDateIsToday(_ date: Date) -> Bool {
        let calendar = Calendar.current
        let today = Date()

        return calendar.isDate(today, inSameDayAs: date)
    }

    // MARK - Private date management methods
    private static func extractCurrentDayNumber() -> Int {
        let date = Date()
        let calendar = Calendar.current
        let numberOfWeek = calendar.component(.weekday, from: date)

        return numberOfWeek
    }

    private static func extractCurrentTime() -> MyTime {
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)

        return MyTime(hour: hour, minute: minute)
    }

    private static func checkStartRange(string: String) -> Bool {
        var result = false
        let startTime = parseStringTime(string: string)

        if currentTime.hour == startTime.hour {
            if currentTime.minute >= startTime.minute {
                result = true
            }
        } else if currentTime.hour > startTime.hour {
            result = true
        }

        return result
    }

    private static func checkEndRange(string: String) -> Bool {
        var result = false
        let endTime = parseStringTime(string: string)

        if currentTime.hour == endTime.hour {
            if currentTime.minute <= endTime.minute {
                result = true
            }
        }
        else if currentTime.hour < endTime.hour  {
            result = true
        }

        return result
    }

    // Sub method of checkStartTime and checkEndTime
    private static func parseStringTime(string: String) -> MyTime {
        let time = string.components(separatedBy: ":")
        let hour = Int(time[0])!
        let minute = Int(time[1])!

        return MyTime(hour: hour, minute: minute)
    }
}

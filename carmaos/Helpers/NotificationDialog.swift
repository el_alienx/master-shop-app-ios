//
//  NotificationDialog.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 10/30/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import FirebaseInstanceID

final class NotificationDialog {
    
    private var application: UIApplication!
    
    // Constructor
    init(application: UIApplication) {
        self.application = application
        
        storeNotificationToken()
    }
    
    func registerRemoteNotifications() {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization( options: authOptions, completionHandler: { granted, error in
            if granted {
                self.storeNotificationToken()
            }
            else {
                print("Notification persmission denied")
            }
        })
        
        self.application.registerForRemoteNotifications()
    }
    
    func storeNotificationToken() {        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                AppStore.user.notificationToken = result.token
            }
        }
    }
}

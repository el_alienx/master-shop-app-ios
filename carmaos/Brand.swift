//
// Created by Eduardo Alvarez on 18/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import UIKit

final class Brand {

    static let store = "carmaos"

    // Colors
    static let color_dominant = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
    static let color_accent = UIColor(red: 0/255, green: 91/255, blue: 189/255, alpha: 1)
    static let color_secondary = UIColor(red: 235/255, green: 235/255, blue: 241/255, alpha: 1)
}

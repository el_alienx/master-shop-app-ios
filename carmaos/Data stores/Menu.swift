//
//  Menu.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 8/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import Firebase

final class Menu {

    var menuCategories: [MenuCategory] = dummyCollection
    var subMenuHeaders: [SubMenuHeader]?
    var products: [[Product]]?

    static let notificationMenu = Notification.Name(rawValue: menuNotificationKey)
    private static let dummyCollection: [MenuCategory] = [
        MenuCategory(data: ["id": "nil", "index": 0, "title": ""]),
        MenuCategory(data: ["id": "nil", "index": 1, "title": ""]),
        MenuCategory(data: ["id": "nil", "index": 2, "title": ""])
    ]
    
    // Constructor
    init(reference: CollectionReference) {
        // Functional

        fetchMenu(reference: reference)
    }

    func fetchMenu(reference: CollectionReference) {
        // Impure mutate state
        // Can be purer if pass the reference (ref: CollRef, target: array)

        let query = reference.order(by: "index")

        FirebaseHandler.readCollection(query: query, completion: { data in
            self.menuCategories = Menu.parseMenuItems(data: data)

            NotificationCenter.default.post(name: Menu.notificationMenu, object: nil)
        })
    }

    private static func parseMenuItems(data: [[String: Any]] ) -> [MenuCategory] {
        // Pure

        return  data.map({ MenuCategory(data: $0) })
    }
}
//
//  Order.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 11/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation

final class ShoppingCart {

    // Refactor, only allow to view it using a getter
    var cartItems: [CartItem] = []
    
    // MARK: - Constructor
    init() {
        //
    }

    // MARK: - Cart management
    func addItemToCart(newItem: CartItem) {
        cartItems.append(newItem)
    }

    func updateItemFromCart(id: String, quantity: Int) {
        if let index = cartItems.index(where: {$0.id == id}) {
            cartItems[index].quantity = quantity
        }
    }
    
    func removeItemFromCartByIndex(at index: Int) {
        cartItems.remove(at: index)
    }

    func removeItemFromCartById(id: String) {
        if let index = cartItems.index(where: {$0.id == id}) {
            cartItems.remove(at: index)
        }
    }
    
    func clearCart() {
        cartItems = []
    }
    
    func getSubtotal() -> Double {
        let items = cartItems.map({ $0.price * Double($0.quantity) })
        
        return items.reduce(0.00, +)
    }
    
    // MARK: - Item management
    func isItemInCart(productId: String) -> Bool {
        return cartItems.contains(where: { $0.id == productId })
    }
    
    func getItemQuantity(productId: String) -> Int? {
        var result: Int?

        if let product = cartItems.first(where: { $0.id == productId }) {
            result = product.quantity
        }

        return result
    }
    
    // MARK: - Helper functions
    func updateCartBadge() -> String? {
        let count = cartItems.count
        let result = (count > 0) ? String(count) : nil
        
        return result
    }
}

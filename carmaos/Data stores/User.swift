//
//  User.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 3/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import CodableFirebase
import Firebase

final class User {

    // Refactor, is time to separate User into User and Auth and Maybe the AddressBook
    // Just like in the MVVM side were Profile and Auth are separated.

    // Auth data
    internal let reference: CollectionReference = FirebaseHandler.getRootReference().collection("users")
    var isReturning = false
    var isLogged = false
    var verificationID: String?
    var verificationCode: String?
    var isNewUser  = false
    var onAuthCompleteSegue: String?
    var notificationToken: String?

    // User data
    var uid: String?
    var name: String?
    var phone: String?
    var addresses: [Address] = []
    var selectedAddress: Int?
    var tempAddress: [String: Any]?

    // Order data (Refactor?)
    var isOrderPlaced = false
    var orders: [[String: Any]] = []
    var latestOrder: [String: Any]?
    var selectedPayment: String?
    
    // Constructor
    init() {
        // Auth
        isReturning = UserDefaults.standard.bool(forKey: "isReturning")
        isLogged = UserDefaults.standard.bool(forKey: "isLogged")
        uid = UserDefaults.standard.string(forKey: "uid")

        // User
        name = UserDefaults.standard.string(forKey: "name")
        phone = UserDefaults.standard.string(forKey: "phone")
        if let addresses = UserDefaults.standard.value(forKey: "addresses") as? [[String: String]] {
            self.addresses = User.decodeAddress(data: addresses)
        }
    }
}

// MARK: - Auth methods
extension User {
    func setIsReturning() {
        // Impure mutate state

        isReturning = true
        UserDefaults.standard.set(isReturning, forKey: "isReturning")
    }

    func createNewUser(uid: String, name: String, phone: String, callBack: @escaping (Bool) -> Void) {
        // Impure depends on state (address)

        let newUser: [String: String] = ["name": name, "phone": phone]

        FirebaseHandler.createDocumentWithId(reference: reference, id: uid, data: newUser, completion: { success in
            if !success {
                callBack(false)
                return
            }

            if let address = self.tempAddress {
                self.storeUserDataLocally()
                self.createAddress(data: address, callBack: callBack)
                return
            }

            self.storeUserDataLocally()
            callBack(true)
        })
    }

    func signIn(uid: String, callBack: @escaping (Bool) -> Void) {
        // Impure mutate state

        let reference = self.reference.document(uid)

        FirebaseHandler.readDocument(reference: reference, completion: { data in
            guard let name = data["name"] as? String else {
                callBack(false)
                return
            }

            guard let phone = data["phone"] as? String else {
                callBack(false)
                return
            }

            self.name = name
            self.phone = phone
            self.storeUserDataLocally()
            self.readAddresses(uid: uid, callBack: callBack)
        })
    }

    func signOut() {
        // Impure mutate state

        isLogged = false
        UserDefaults.standard.set(isLogged, forKey: "isLogged")

        uid = nil
        name = nil
        phone = nil
        addresses = []
        UserDefaults.standard.set(uid, forKey: "addresses")
        UserDefaults.standard.set(name, forKey: "name")
        UserDefaults.standard.set(phone, forKey: "phone")
        UserDefaults.standard.set(addresses, forKey: "addresses")        
    }
}

// MARK: - Address methods
extension User {
    // Create
    func createAddress(data: [String: Any], callBack: @escaping (Bool) -> Void ) {
        let reference = self.reference.document(uid!).collection("addresses")

        FirebaseHandler.createNewDocument(reference: reference, data: data, completion: { success, id in
            if !success {
                callBack(false)
                return
            }

            var newAddressData: [String: Any] = data
            newAddressData["id"] = id
            self.addresses.append(Address(data: newAddressData))
            self.storeAddressesLocally(addresses: self.addresses)
            callBack(true)
        })
    }

    // Read
    func readAddresses(uid: String, callBack: @escaping (Bool) -> Void ) {
        let reference = self.reference.document(uid).collection("addresses").order(by: "urbanization")

        FirebaseHandler.readCollection(query: reference, completion: { data in
            self.addresses = User.decodeAddress(data: data)
            self.storeAddressesLocally(addresses: self.addresses)
            callBack(true)
        })
    }

    // Update
    func updateAddress(id: String, data: [String: Any], callBack: @escaping (Bool) -> Void ) {
        let reference = self.reference.document(uid!).collection("addresses")

        FirebaseHandler.updateDocument(reference: reference, id: id, data: data, completion: { success in
            if let offset = self.addresses.index(where: {$0.id == id}) {
                self.addresses[offset] = Address(data: data)
            }

            self.storeAddressesLocally(addresses: self.addresses)
            callBack(success)
        })
    }

    // Delete
    func deleteAddress(index: Int, callBack: @escaping (Bool) -> Void ) {
        let address = addresses[index]
        let reference = self.reference.document(uid!).collection("addresses")
        
        guard let id = address.id else {
            print("User.swift - This address does not have an id")
            return
        }
        
        FirebaseHandler.deleteDocument(reference: reference, id: id, completion: { success in
            if !success {
                callBack(false)
                return
            }

            self.addresses.remove(at: index)
            self.storeAddressesLocally(addresses: self.addresses)
            callBack(true)
        })
    }

    // Helpers
    private func storeUserDataLocally() {
        // Impure mutate state

        isLogged = true
        UserDefaults.standard.set(isLogged, forKey: "isLogged")

        UserDefaults.standard.set(uid, forKey: "uid")
        UserDefaults.standard.set(name, forKey: "name")
        UserDefaults.standard.set(phone, forKey: "phone")
    }

    private func storeAddressesLocally(addresses: [Address]) {
        var  result: [[String: Any]] = []

        for (index, _) in addresses.enumerated() {
            let item = try! FirestoreEncoder().encode(addresses[index])
            result.append(item)
        }

        UserDefaults.standard.set(result, forKey: "addresses")
    }

    static private func decodeAddress(data: [[String: Any]]) -> [Address] {
        return data.map({ Address(data: $0) })
    }
}

// MARK: - Orders methods
extension User {
    func readOrders(callBack: @escaping () -> Void) {
        let query = self.reference.document(uid!).collection("orders").document(Brand.store).collection("orders")

        FirebaseHandler.readCollection(query: query, completion: { data in
            self.orders = self.sortDictionaries(data: data, key: "date", ascending: false)
            callBack()
        })
    }

    func readLatestOrder(callBack: @escaping (Bool) -> Void) {
        let query = self.reference.document(uid!).collection("orders").document(Brand.store).collection("orders")
                .order(by: "date", descending: true).limit(to: 1)

        FirebaseHandler.readCollection(query: query, completion: { data in
            if data.count == 0 {
                callBack(false)
                return
            }

            if !self.validateLatestOrder(data: data[0]) {
                callBack(false)
                return
            }

            self.latestOrder = data[0]
            callBack(true)
        })
    }

    // Helper
    func validateLatestOrder(data: [String: Any]) -> Bool {
        // Put all validation rules here
        let date = data["date"] as! Date
        let status = data["status"] as! Int

        if !DateTimeHandler.checkIfDateIsToday(date) {
            return false
        }

        // Refactor: Status 3 equals delivered. Move to a enum or array of code statuses
        if status == 3 {
            return false
        }

        return true
    }

    func sortDictionaries(data: [[String: Any]], key: String, ascending: Bool) -> [[String: Any]] {
        return (data as NSArray).sortedArray(using: [NSSortDescriptor(key: key, ascending: ascending)])
                as! [[String: Any]]
    }
}

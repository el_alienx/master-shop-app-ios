//
//  Restaurant.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 8/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import Firebase

final class Restaurant {
    
    let transactionalPercentage: Double = 0.16
    var paymentMethods: [String]?
    var shippingCost: Double?
    var schedule: [String: [String]]?
    var isStoreOpen: Bool { return checkIfOpen() }

    static let notification = Notification.Name(rawValue: restaurantNotificationKey)

    // Construct
    init(reference: DocumentReference) {
        // Functional
        
        fetch(reference: reference)
    }
    
    func fetch(reference: DocumentReference) {
        // Impure mutate state

        FirebaseHandler.readDocument(reference: reference, completion: { data in
            self.paymentMethods = data["payment_methods"] as? [String]
            self.shippingCost = data["shipping_cost"] as? Double
            self.schedule = data["schedule"] as? [String: [String]]

            NotificationCenter.default.post(name: Restaurant.notification, object: nil)
        })
    }

    func checkIfOpen() -> Bool {
        let isWeekDay = DateTimeHandler.checkIfTodayIsWeekday()
        let selectedSchedule = isWeekDay ? "weekdays" : "weekends"

        guard schedule != nil else {
            return false
        }

        return DateTimeHandler.checkIfTimeRange(schedule: schedule![selectedSchedule]!)
    }
}

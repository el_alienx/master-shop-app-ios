//
//  VerifyCodeView.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 19/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

final class VerifyCodeView: UIViewController {

    private var viewModel: VerifyCodePresenter!

    // Poner glyph
    @IBOutlet weak var myGlyph: UIView!
    @IBOutlet weak var codeTextField: MSTextField!
    @IBOutlet weak var myMainButton: UIButton!
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let user = AppStore.user!

        viewModel = VerifyCodePresenter(model: VerifyCodeModel(user: user))

        setView()
    }

    func setView() {
        let length = viewModel.maxLength

        myGlyph.tintColor = Brand.color_accent
        codeTextField.start(title: "Código SMS (\(length) números)", placeholder: "123456", maxLength: length)
        codeTextField.setKeyboardType(keyboardType: .numberPad)
        codeTextField.requiredLength = true
        codeTextField.requiredNumber = true
    }
    
    func callBack(success: Bool, uid: String, isNewUser: Bool) {
        if !success {
            showAlert(title: "No pudimos confirmar tu celular", message: "Revisa tu celular y te enviaremos un nuevo código")
            enableButton()
            return
        }

        viewModel.uid = uid
        viewModel.isNewUser = isNewUser

        performSegue(withIdentifier: "showProfileLoader", sender: nil)
    }

    @IBAction func mainButton(_ sender: UIButton) {
        if !codeTextField.validate() {
            return
        }

        let verificationID = viewModel.verificationID
        let verificationCode = codeTextField.text

        disableButton()
        viewModel.confirmVerificationSMS(
                verificationID: verificationID!,
                verificationCode: verificationCode!,
                callBack: callBack)
    }
}

extension VerifyCodeView {

    // Refactor, give these properties to MSButton on refactor
    func enableButton() {
        myMainButton.isEnabled = true
        myMainButton.alpha = 1
        myActivityIndicator.stopAnimating()
    }

    func disableButton() {
        myMainButton.isEnabled = false
        myMainButton.alpha = 0
        myActivityIndicator.startAnimating()
    }
}

extension VerifyCodeView {

    // Refactor make available in all View Controller classes
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
}

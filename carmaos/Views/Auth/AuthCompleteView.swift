//
//  AuthCompleteView.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 22/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class AuthCompleteView: UIViewController {
    
    private var viewModel: AuthCompletePresenter!

    @IBOutlet weak var myGlyph: UIImageView!
    @IBOutlet weak var myInstructions: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let user = AppStore.user!

        viewModel = AuthCompletePresenter(model: AuthCompleteModel(user: user) )

        setView()
    }
    
    func setView() {
        navigationItem.hidesBackButton = true
        myGlyph.tintColor = Brand.color_accent

        viewModel.loadProfile(callBack: callBack)
    }

    func callBack(success: Bool) {
        if !success {
            // Refactor UIAlert
            print("ProfileLoader.swift - Could not load profile")
            return
        }

        profileLoaded()
    }

    func profileLoaded() {
        if viewModel.onAuthCompleteSegue == "showCheckout" {
            performSegue(withIdentifier: "showCheckout", sender: nil)
            return
        }

        navigationController?.popToRootViewController(animated: true)
    }
}

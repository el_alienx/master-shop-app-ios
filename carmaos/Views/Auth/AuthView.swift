//
//  AuthView.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 19/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

final class AuthView: UIViewController {

    private var viewModel: AuthPresenter!
    var receivedCredential: String?
    
    @IBOutlet weak var myGlyph: UIImageView!
    @IBOutlet weak var nameTextField: MSTextField!
    @IBOutlet weak var phoneTextField: MSTextField!
    @IBOutlet weak var myMainButton: UIButton!
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let user = AppStore.user!

        viewModel = AuthPresenter(model: AuthModel(user: user) )

        setView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        enableButton()
    }

    func setView() {
        myGlyph.tintColor = Brand.color_accent

        setForm()

        if receivedCredential == "SignIn" {
            myGlyph.image = UIImage(named: "glyph-sign-in")
        }
    }

    func callBack(verificationID: String) {
        viewModel.verificationID = verificationID

        showVerify()
    }

    func setForm() {
        nameTextField.start(title: "Nombre y Apellido", placeholder: "Eduardo Alvarez", maxLength: 25)
        phoneTextField.start(title: "Celular", placeholder: "0999065560", maxLength: viewModel.maxLength)
        phoneTextField.setKeyboardType(keyboardType: .phonePad)

        nameTextField.requiredName = true
        phoneTextField.requiredNumber = true
        phoneTextField.requiredLength = true
        phoneTextField.requiredPhoneBasic = true

        if receivedCredential == "SignIn" {
            nameTextField.removeFromSuperview()
        }
    }

    func showVerify() {
        performSegue(withIdentifier: "showVerifyPhone", sender: nil)
    }

    func saveUser(name: String, phone: String) {
        viewModel.name = name
        viewModel.phone = phone
    }

    // Refactor, give these properties to MSButton on refactor
    func enableButton() {
        myMainButton.isEnabled = true
        myMainButton.alpha = 1
        myActivityIndicator.stopAnimating()
    }
    func disableButton() {
        myMainButton.isEnabled = false
        myMainButton.alpha = 0
        myActivityIndicator.startAnimating()
    }

    func authCreate() {
        print("authCreate")

        if !nameTextField.validate() { return }
        if !phoneTextField.validate() { return }

        let name = nameTextField.text!
        let phone = phoneTextField.text!

        saveUser(name: name, phone: phone)
        viewModel.sendVerificationSMS(phone: phone, callBack: callBack)
        disableButton()
    }

    func authSignIn() {
        if !phoneTextField.validate() { return }

        let phone = phoneTextField.text!

        viewModel.sendVerificationSMS(phone: phone, callBack: callBack)
        disableButton()
    }

    @IBAction func mainButton(_ sender: UIButton) {
        if receivedCredential == "SignIn" {
            authSignIn()
            return
        }

        authCreate()
    }
}

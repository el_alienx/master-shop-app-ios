//
//  OrderHistoryView.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 23/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class OrderHistoryView: UIViewController {

    internal var viewModel: OrderHistoryPresenter!
    internal var selectedOrder: [String: Any]?

    @IBOutlet weak var myGlyph: UIImageView!
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var stateView: UIView!
    @IBOutlet weak var stateGlyph: UIImageView!
    @IBOutlet weak var stateText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let user: User = AppStore.user
        
        viewModel = OrderHistoryPresenter(model: OrderHistoryModel(user: user))
        
        setView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel.readOrders(callBack: callBack)
    }

    func setView() {
        myGlyph.tintColor = Brand.color_accent
        myTableView.tableFooterView = UIView()
        myTableView.alwaysBounceVertical = false
    }

    func callBack() {
        updateView()
    }

    func updateView() {
        if (viewModel.orders.count == 0) {
            stateGlyph.tintColor = Brand.color_accent
            stateText.text = viewModel.formatStateMessage()
            addStateView(stateView: stateView)
            return
        }
        
        stateView.removeFromSuperview()
        myActivityIndicator.stopAnimating()
        myTableView.reloadData()
    }
    
    func addStateView(stateView: UIView){
        stateView.frame.size.width = view.safeAreaLayoutGuide.layoutFrame.width
        stateView.frame.size.height = view.safeAreaLayoutGuide.layoutFrame.height
        stateView.frame.origin = CGPoint(x: 0, y: view.safeAreaLayoutGuide.layoutFrame.minY)
        
        view.addSubview(stateView)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationPage = segue.destination as! OrderDetailView

        destinationPage.receivedOrder = selectedOrder
    }
    
    @IBAction func stateButton(_ sender: UIButton) {
        // selectedIndex = 0 is Menu
        tabBarController?.selectedIndex = 0
    }
}

extension OrderHistoryView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.orders.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        let item: [String: Any] = viewModel.orders[indexPath.row]

        cell.textLabel?.text = viewModel.showDate(date: item["date"]!)
        cell.detailTextLabel?.text = viewModel.showTotal(amount: item["total_price"] as! Double)

        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedOrder = viewModel.orders[indexPath.row]

        self.performSegue(withIdentifier: "showDetail", sender: UITableView.self)
    }
}

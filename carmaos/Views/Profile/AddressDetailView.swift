//
//  NewAddressView.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 23/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class AddressDetailView: UIViewController {

    private var viewModel: AddressDetailPresenter!
    var receivedAddress: Address?
    var receivedCallBack: (() -> Void)?

    @IBOutlet weak var myGlyph: UIImageView!
    @IBOutlet weak var urbanizationTextField: MSTextField!
    @IBOutlet weak var streetTextField: MSTextField!
    @IBOutlet weak var numerationTextField: MSTextField!
    @IBOutlet weak var referenceTextView: MSTextView!
    @IBOutlet weak var myMainButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let user: User = AppStore.user

        viewModel = AddressDetailPresenter(model: AddressDetailModel(user: user))

        setView()
    }

    func setView() {
        let addButton: UIBarButtonItem?

        if receivedAddress != nil {
            addButton = UIBarButtonItem(barButtonSystemItem: .done, target: self,
                    action: #selector(setAddress(sender:)))
            myMainButton.setTitle("Actualizar", for: .normal)
        }
        else {
            addButton = UIBarButtonItem(barButtonSystemItem: .save, target: self,
                    action: #selector(setAddress(sender:)))
            myMainButton.setTitle("Guardar", for: .normal)
        }

        myGlyph.tintColor = Brand.color_accent
        navigationItem.rightBarButtonItem = addButton
        setForm()
    }

    func setForm() {
        let adr = Address.self
        
        urbanizationTextField.start(title: adr.urbTitle, placeholder: adr.urbPlaceholder, maxLength: 30)
        streetTextField.start(title: adr.stTitle, placeholder: adr.stPlaceholder, maxLength: 35)
        numerationTextField.start(title: adr.numTitle, placeholder: adr.numPlaceholder, maxLength: 30)
        referenceTextView.start(title: adr.refTitle, placeholder: adr.refPlaceholder, maxLength: 140)

        urbanizationTextField.required = true
        streetTextField.required = true
        numerationTextField.required = true

        if let address = receivedAddress {
            urbanizationTextField.text = address.urbanization
            streetTextField.text = address.street
            numerationTextField.text = address.numeration
            referenceTextView.text = address.reference
        }
    }

    func callBack(success: Bool) {
        if !success {
            // Refactor UIAlert
            print("AddressDetailView.swift callBack() Could not save or update address")
            return
        }

        receivedCallBack?()
        navigationController?.popViewController(animated: true)
    }

    @objc func setAddress(sender: AnyObject) {
        if !urbanizationTextField.validate() { return }
        if !streetTextField.validate() { return }
        if !numerationTextField.validate() { return }

        var data: [String: Any] = [
            "urbanization": urbanizationTextField.text!,
            "street": streetTextField.text!,
            "numeration": numerationTextField.text!,
            "reference": referenceTextView.text!
        ]

        if let myId = receivedAddress?.id {
            data["id"] = myId
        }

        viewModel.setAddress(data: data, callBack: callBack)
    }

    @IBAction func mainButton(_ sender: UIButton) {
        setAddress(sender: self)
    }
}

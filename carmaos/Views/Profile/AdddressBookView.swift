//
//  AdddressBookView.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 23/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class AddressBookView: UIViewController {

    private var viewModel: AddressBookPresenter!
    var selectedAddress: Address?

    @IBOutlet weak var myGlyph: UIImageView!
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet var stateView: UIView!
    @IBOutlet weak var stateGlyph: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let user: User = AppStore.user
        
        viewModel = AddressBookPresenter(model: AddressBookModel(user: user))
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        setView()
        updateView()
    }

    func setView() {
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(showAddress(sender:)))
        navigationItem.rightBarButtonItem = addButton

        myGlyph.tintColor = Brand.color_accent
        myTableView.tableFooterView = UIView()
        myTableView.alwaysBounceVertical = false
    }
    
    func updateView() {
        if (viewModel.addresses.count == 0) {
            stateGlyph.tintColor = Brand.color_accent
            addStateView(stateView: stateView)
            return
        }

        stateView.removeFromSuperview()
        myTableView.reloadData()
    }

    func callBackRead() {
        updateView()
    }

    func callBackDelete(success: Bool) {
        if !success {
            // Refactor UIAlert
            print("AddressBookView.swift - Could not delete address")
            return
        }

        updateView()
    }
    
    func addStateView(stateView: UIView){
        stateView.frame.size.width = view.safeAreaLayoutGuide.layoutFrame.width
        stateView.frame.size.height = view.safeAreaLayoutGuide.layoutFrame.height
        stateView.frame.origin = CGPoint(x: 0, y: view.safeAreaLayoutGuide.layoutFrame.minY)
        
        view.addSubview(stateView)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationPage = segue.destination as! AddressDetailView

        destinationPage.receivedCallBack = updateView
        destinationPage.receivedAddress = selectedAddress
    }

    @objc func showAddress(sender: AnyObject) {
        selectedAddress = nil
        performSegue(withIdentifier: viewModel.segue, sender: nil)
    }

    @IBAction func mainButton(_ sender: UIButton) {
        showAddress(sender: self)
    }
    
    @IBAction func stateButton(_ sender: UIButton) {
        showAddress(sender: self)
    }
}

extension AddressBookView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.addresses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        let item: Address? = viewModel.addresses[indexPath.row]

        cell.textLabel?.text = item?.urbanization

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedAddress = viewModel.addresses[indexPath.row]

        self.performSegue(withIdentifier: viewModel.segue, sender: UITableView.self)
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel.deleteAddress(index: indexPath.row, callBack: callBackDelete)
        }
    }
}

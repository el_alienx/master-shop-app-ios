//
//  ProfileViewController.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 3/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

final class ProfileView: UIViewController {
    
    private var viewModel: ProfilePresenter!
    private var selectedAction: String?
    
    @IBOutlet weak var myStateView: UIView!
    @IBOutlet weak var myWelcomeMessage: UILabel!
    @IBOutlet weak var myGlyph: UIImageView!
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var orderButton: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        let user: User = AppStore.user

        viewModel = ProfilePresenter(model: ProfileModel(user: user) )
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        setView()
    }

    func setView() {
        if !viewModel.isLogged {
            myGlyph.tintColor = Brand.color_accent
            addStateView(stateView: myStateView)
            return
        }

        viewModel.readLatestOrder(callBack: callBack)
        
        myActivityIndicator.startAnimating()
        orderButton.alpha = 0
        orderButton.setImage(UIImage(named: "order-button-disabled.png"), for: .disabled)
        myStateView.removeFromSuperview()
        myWelcomeMessage.text = "Bienvenido \(viewModel.name!)"
        myTableView.tableFooterView = UIView()
    }

    func callBack(success: Bool) {
        handleLatestOrder(success: success)
    }

    func addStateView(stateView: UIView){
        stateView.frame.size.width = view.safeAreaLayoutGuide.layoutFrame.width
        stateView.frame.size.height = view.safeAreaLayoutGuide.layoutFrame.height
        stateView.frame.origin = CGPoint(x: 0, y: view.safeAreaLayoutGuide.layoutFrame.minY)

        view.addSubview(stateView)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == viewModel.authSegue {
            let destinationPage = segue.destination as! AuthView
            destinationPage.receivedCredential = selectedAction
            return
        }

        if segue.identifier == "showOrderDetail" {
            let destinationPage = segue.destination as! OrderDetailView
            destinationPage.receivedOrder = viewModel.latestOrder
            return
        }
    }
    
    @IBAction func signOutButton(_ sender: UIButton) {
        viewModel.signOut()
        addStateView(stateView: myStateView)
    }
    
    @IBAction func myStateMain(_ sender: UIButton) {
        selectedAction = "Create"
        performSegue(withIdentifier: viewModel.authSegue, sender: nil)
    }

    // Note: This will disappear if we unify the login
    @IBAction func myStateSecondary(_ sender: UIButton) {
        selectedAction = "SignIn"
        performSegue(withIdentifier: viewModel.authSegue, sender: nil)
    }
}

// Table View
extension ProfileView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.menuOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell  = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        let item: [String: String] = viewModel.menuOptions[indexPath.row]

        cell.textLabel?.text = item["label"]
        cell.imageView?.image = UIImage(named: item["icon"]!)
        cell.imageView?.tintColor = Brand.color_dominant

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item: [String: String] = viewModel.menuOptions[indexPath.row]
        let segue: String = item["segue"]!

        self.performSegue(withIdentifier: segue, sender: UITableView.self)
    }
}

// Micro State
extension ProfileView {
    func handleLatestOrder(success: Bool) {
        myActivityIndicator.stopAnimating()
        orderButton.alpha = 1
        orderButton.isEnabled = success
    }

    @IBAction func orderButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showOrderDetail", sender: UITableView.self)
    }
}

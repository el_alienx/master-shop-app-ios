//
//  OrderDetailView.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 23/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class OrderDetailView: UIViewController {

    private var viewModel: OrderDetailPresenter!
    var receivedOrder: [String: Any]?

    // Order status bar
    @IBOutlet weak var statusBarText: UILabel!
    @IBOutlet weak var statusBarIcon: UIImageView!
    @IBOutlet weak var statusBarColor: UIView!

    // Shopping cart
    @IBOutlet weak var myTitleDate: UILabel!
    @IBOutlet weak var myTableView: UITableView!
    
    // Payment details
    @IBOutlet weak var myTotal: UILabel!
    @IBOutlet weak var mySubTotal: UILabel!
    @IBOutlet weak var myShippingCost: UILabel!
    @IBOutlet weak var myTransactionStackView: UIStackView!
    @IBOutlet weak var myTransactionCost: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard let order = receivedOrder else {
            print("OrderDetailView.swift - No order received")
            return
        }

        let user: User = AppStore.user

        viewModel = OrderDetailPresenter(model: OrderDetailModel(user: user, order: order))

        setView()
    }
    
    func setView() {
        myTitleDate.text = viewModel.showDate(date: viewModel.order["date"]!)
        myTableView.tableFooterView = UIView()
        myTableView.alwaysBounceVertical = false
        
        setStatusBar(code: viewModel.statusCode)
        setPaymentCost()
    }
    
    func setStatusBar(code: Int) {
        let vm = viewModel!
        let text = vm.statusBarContent[code]["text"] as? String
        let icon = vm.statusBarContent[code]["icon"] as! String
        let color = vm.statusBarContent[code]["color"] as? UIColor
        
        statusBarIcon.tintColor = Color.white
        statusBarText.text = text
        statusBarIcon.image = UIImage(named: icon)
        statusBarColor.backgroundColor = color
    }
    
    func setPaymentCost() {
        let vm = viewModel!
        let total = vm.order["total_price"] as! Double
        let subTotal = vm.order["sub_total"] as! Double
        let shippingCost = vm.order["shipping_cost"] as! Double
        let transactionalCost = vm.order["transactional_cost"] as! Double
        
        myTotal.text = vm.formatPrice(amount: total)
        mySubTotal.text = vm.formatPrice(amount: subTotal)
        myShippingCost.text = vm.formatPrice(amount: shippingCost)
        myTransactionCost.text = vm.formatPrice(amount: transactionalCost)
        
        if (transactionalCost == 0) {
            myTransactionStackView.removeFromSuperview()
        }
    }
}

extension OrderDetailView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let array = receivedOrder!["shopping_cart"] as! [[String : Any]]
        
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartCell
        let array = receivedOrder!["shopping_cart"] as! [[String : Any]]
        let item = array[indexPath.row]

        let name = item["name"] as? String
        let image = item["image"] as! String
        let price = item["price"] as! Double
        let quantity = item["quantity"] as! Int

        cell.price?.text = viewModel.formatItemTotal(price: price, quantity: quantity)
        cell.subtitle?.text = viewModel.formatItemUnits(quantity: quantity)
        cell.title?.text = name
        cell.thumbnail?.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "placeholder.png"))

        return cell
    }
}

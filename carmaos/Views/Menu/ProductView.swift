//
//  Detail.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 27/7/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit
import SDWebImage

final class ProductView: UIViewController {
    
    internal var viewModel: ProductPresenter!
    internal var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    var receivedDocument: Product?
    var receivedCallback: (() -> ())?

    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var myTitle: UILabel!
    @IBOutlet weak var myDescription: UILabel!
    @IBOutlet weak var myPrice: UILabel!
    @IBOutlet weak var myUnits: UILabel!
    @IBOutlet weak var myAddToCardButton: UIButton!
    
    // Micro States
    @IBOutlet weak var myGlyph: UIImageView!
    @IBOutlet weak var MicroStateContainer: UIView!
    @IBOutlet var myMicroState: UIView!
    @IBOutlet var myCartController: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let document = receivedDocument {
            let shoppingCart = AppStore.shoppingCart!
            let restaurant = AppStore.restaurant!

            viewModel = ProductPresenter(model: ProductModel(
                product: document,
                shoppingCart: shoppingCart,
                restaurant: restaurant
            ))

            setView()
        }
        else {
            print("ProductView.swift - No document received")
        }
    }
    
    func setView() {
        let microState:UIView = viewModel.isStoreOpen ? myCartController : myMicroState

        myGlyph.tintColor = Brand.color_accent
        myTitle.text = viewModel.product.name
        myDescription.text = viewModel.product.description
        myImage.sd_setImage(with: URL(string: viewModel.product.image!),
                placeholderImage: UIImage(named: viewModel.product.thumbnail!))

        MicroStateContainer.addSubview(microState)

        updateView()
    }
    
    func updateView() {
        myPrice.text = viewModel.showSubTotal()
        myUnits.text = viewModel.formatUnit(quantity: viewModel.quantity)

        updateMainButton()
    }
    
    @IBAction func closeCircleButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

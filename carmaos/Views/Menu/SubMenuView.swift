 //
 //  FoodSubMenuViewController
 //  carmaos
 //
 //  Created by Eduardo Alvarez on 19/7/18.
 //  Copyright © 2018 Eduardo Alvarez. All rights reserved.
 //
 
 import UIKit
 import SDWebImage
 
 final class SubMenuView: UIViewController {

    internal var viewModel: SubMenuPresenter!
    var receivedDocument: MenuCategory?

    @IBOutlet weak var myShortcutsView: UICollectionView!
    @IBOutlet weak var myProductsView: UICollectionView!
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let document = receivedDocument else {
            print("SubMenu.swift - No document received")
            return
        }
        
        let categoryId = document.id
        let reference = FirebaseHandler.getStoreReference().collection("menu").document(categoryId).collection("subcollection")

        viewModel = SubMenuPresenter(model: SubMenuModel(receivedDocument: document, reference: reference))
        viewModel.loadCollection(callback: dataLoadedCallback)
        
        setView()
    }
    
    func dataLoadedCallback () -> () {
        updateView()
    }
    
    func modalClosedCallBack () -> () {
        tabBarController?.tabBar.items?[1].badgeValue = viewModel.updateCartTabBadge()
        updateView()
    }
    
    func setView() {
        navigationItem.title = receivedDocument!.title
        
        myProductsView.allowsSelection = false
        myProductsView.alpha = 0
        myActivityIndicator.startAnimating()
        
        setCollectionViewCell()
    }
    
    func updateView() {
        navigationItem.title = receivedDocument!.title
        
        myProductsView.reloadData()
        myProductsView.alpha = 1
        myProductsView.allowsSelection = true
        myActivityIndicator.stopAnimating()
    }
    
    func changeView(newMenuItem: MenuCategory) {
        receivedDocument = newMenuItem
        
        viewModel.reference = FirebaseHandler.getStoreReference().collection("menu").document(newMenuItem.id).collection("subcollection")
        viewModel.loadCollection(callback: dataLoadedCallback)
        
        setView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationPage = segue.destination as! ProductView
        
        destinationPage.receivedDocument = viewModel.selectedDocument
        destinationPage.receivedCallback = modalClosedCallBack
    }
 }

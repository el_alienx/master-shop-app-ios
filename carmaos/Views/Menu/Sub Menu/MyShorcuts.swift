//
//  MyShorcuts.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 5/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

extension SubMenuView {    
    func shortcutCellForItemAt( collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardMini", for: indexPath) as! CardSmall
        let item =  viewModel.menuCategories[indexPath.row]

        cell.title?.text = item.title
        cell.thumbnail.sd_setImage(with: URL(string: item.thumbnail), placeholderImage: UIImage(named: item.image))
        
        return cell
    }
    
    func shortcutDidSelectItemAt(collectionView: UICollectionView, indexPath: IndexPath) {
        let menuItem = AppStore.menu?.menuCategories[indexPath.row]
        
        changeView(newMenuItem: menuItem!)
    }
}

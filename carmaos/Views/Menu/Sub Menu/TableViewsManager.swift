//
//  TableViewsManager.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 15/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

extension SubMenuView: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if (collectionView == myShortcutsView) {
            return 1
        }
        else {
            return viewModel.collections.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == myShortcutsView) {
            return viewModel.menuCategories.count
        }
        else {
            return categoriesNumberOfItemsInSection(section: section)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == myShortcutsView) {
            return shortcutCellForItemAt(collectionView: collectionView, indexPath: indexPath)
        }
        else {
            return categoriesCellForItemAt(collectionView: collectionView, indexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == myShortcutsView) {
            shortcutDidSelectItemAt(collectionView: collectionView, indexPath: indexPath)
        }
        else {
            categoriesDidSelectItemAt(collectionView: collectionView, indexPath: indexPath)
        }
    }
}


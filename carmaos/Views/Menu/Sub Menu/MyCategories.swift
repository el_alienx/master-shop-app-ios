//
//  MyCategories.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 5/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

extension SubMenuView {
    // Refactor
    // Move to CardSmall
    func setCollectionViewCell() {
        let layout = myProductsView.collectionViewLayout as! UICollectionViewFlowLayout
        let margin: CGFloat = 16
        let columnGap: CGFloat = 17
        let responsiveSquare = (myProductsView.frame.size.width - margin - margin - columnGap) / 2

        layout.itemSize = CGSize(width: responsiveSquare, height: responsiveSquare + 60)
    }
    
    func categoriesNumberOfItemsInSection(section: Int) -> Int {
        let multiArray:[[String: Any]]? = viewModel?.collections[section]["collection"] as? [[String : Any]]
        let result = (multiArray != nil) ? multiArray?.count : 0
        
        return result!
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeader", for: indexPath) as! SectionHeader
        let title = viewModel?.collections[indexPath.section]["title"] as? String
        
        header.title?.text = title
        
        return header
    }
    
    func categoriesCellForItemAt( collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardSmall", for: indexPath) as! CardMedium
        
        var multiArray: [[String: Any]] = viewModel!.collections[indexPath.section]["collection"] as! [[String : Any]]
        var item: [String: Any] = multiArray[indexPath.row]

        let id = item["id"] as? String
        let title = item["title"] as? String
        let thumbnail = item["image"] as? String ?? ""
        let checkAlpha: CGFloat = viewModel.isItemInCart(productId: id!) ? 1 : 0
        
        cell.title?.text = title
        cell.thumbnail.sd_setImage(with: URL(string: thumbnail), placeholderImage: UIImage(named: "placeholder.png"))
        cell.check.alpha = checkAlpha
        cell.checkCircle.alpha = checkAlpha
        
        return cell
    }
    
    func categoriesDidSelectItemAt(collectionView: UICollectionView, indexPath: IndexPath) {
        let multiArray:[[String: Any]] = viewModel.collections[indexPath.section]["collection"] as! [[String : Any]]
        let selectedItem = multiArray[indexPath.row]
        
        viewModel?.selectedDocument = Product(data: selectedItem)
        
        self.performSegue(withIdentifier: "showProductFromMenu", sender: nil)
    }
}

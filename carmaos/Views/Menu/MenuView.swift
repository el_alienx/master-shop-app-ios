//
//  FoodMenuViewController
//  carmaos
//
//  Created by Eduardo Alvarez on 19/7/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit
import SDWebImage

final class MenuView: UIViewController {
    
    private var viewModel: MenuPresenter!
    private let notification = Notification.Name(rawValue: menuNotificationKey)
    
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let menu = AppStore.menu!
        
        viewModel = MenuPresenter(model: MenuModel(menu: menu))
        createObserver()
        setView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setCollectionViewCell()
    }
    
    func setView() {
        if viewModel.collection[0].id == "nil" {
            myCollectionView.allowsSelection = false
        }
    }
    
    func createObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(MenuView.updateView),
                                               name: notification, object: nil)
    }
    
    @objc func updateView() {
        myCollectionView.reloadData()
        myCollectionView.allowsSelection = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationPage = segue.destination as! SubMenuView
        
        destinationPage.receivedDocument = viewModel.selectedDocument
    }
}

extension MenuView: UICollectionViewDataSource, UICollectionViewDelegate {
    func setCollectionViewCell() {
        let layout = myCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let margin: CGFloat = 16
        let responsiveWidth = (myCollectionView.frame.size.width - margin - margin)
        let aspectRatio16_10: CGFloat = 1.6

        print("responsive width: \(responsiveWidth)")
        
        layout.itemSize = CGSize(width: responsiveWidth, height: responsiveWidth / aspectRatio16_10)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.collection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardBig", for: indexPath) as! CardBig
        let menuItem = viewModel.collection[indexPath.row]
        
        cell.title?.text = menuItem.title
        cell.subtitle?.text = (menuItem.subtitle).uppercased()
        cell.thumbnail.sd_setImage(with: URL(string: menuItem.image), placeholderImage: UIImage(named: "placeholder.png"))
        cell.setupComponent()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.selectedDocument = viewModel.collection[indexPath.row]
        
        self.performSegue(withIdentifier: "showSubMenu", sender: nil)
    }
}

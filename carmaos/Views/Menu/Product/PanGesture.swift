//
//  PanGesture.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 5/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

extension ProductView {
    func gestureChanged(point: CGPoint) {
        if point.y - initialTouchPoint.y > 0 {
            self.view.frame = CGRect(x: 0, y: point.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
        }
    }
    
    func gestureEnded(point: CGPoint) {
        if point.y - initialTouchPoint.y > 100 {
            self.dismiss(animated: true, completion: nil)
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            })
        }
    }
    
    @IBAction func panGestureRecognizer(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        switch sender.state {
        case .possible:
            break
        case .began:
            initialTouchPoint = touchPoint
        case .changed:
            gestureChanged(point: touchPoint)
        case .ended:
            gestureEnded(point: touchPoint)
        case .cancelled:
            gestureEnded(point: touchPoint)
        case .failed:
            break
        }
    }
}

//
//  AddToCartControllers.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 5/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

extension ProductView {

    func updateMainButton() {
        let quantity: Int = viewModel.quantity

        func defaultColors() {
            myAddToCardButton.setTitleColor(.white, for: .normal)
            myAddToCardButton.backgroundColor = Brand.color_dominant
        }

        guard let quantityInCart: Int = viewModel.quantityInCart else {
            myAddToCardButton.setTitle("Agregar al Carrito", for: .normal)
            defaultColors()
            return
        }

        if quantity == quantityInCart {
            myAddToCardButton.setTitle("Ya estoy en el Carrito", for: .normal)
            myAddToCardButton.setTitleColor(Brand.color_dominant, for: .normal)
            myAddToCardButton.backgroundColor = Brand.color_secondary
        }

        if quantity > quantityInCart {
            myAddToCardButton.setTitle("Aumentar unidades", for: .normal)
            defaultColors()
        }

        if quantity < quantityInCart {
            myAddToCardButton.setTitle("Disminuir unidades", for: .normal)
            defaultColors()
        }

        if quantity == 0 {
            myAddToCardButton.setTitle("Quitar del Carrito", for: .normal)
            myAddToCardButton.setTitleColor(.white, for: .normal)
            myAddToCardButton.backgroundColor = Color.isDanger
        }
    }

    @IBAction func addItem(_ sender: UIButton) {
        viewModel.increaseUnits()
        updateView()
    }
    
    @IBAction func removeItem(_ sender: UIButton) {
        viewModel.decreaseUnits()
        updateView()
    }
    
    @IBAction func addToCardButton(_ sender: UIButton) {
        let quantity: Int = viewModel.quantity

        guard let quantityInCart: Int = viewModel.quantityInCart else {
            viewModel.addItemToCart()

            receivedCallback?()
            self.dismiss(animated: true, completion: nil)
            return
        }

        if quantity == quantityInCart {
            // do nothing
        }

        if quantity > quantityInCart {
            viewModel.updateItemFromCart()
        }

        if quantity < quantityInCart {
            viewModel.updateItemFromCart()
        }

        if quantity == 0 {
            viewModel.removeItemFromCart()
        }

        receivedCallback?()
        self.dismiss(animated: true, completion: nil)
    }
}

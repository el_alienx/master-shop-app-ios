//
// Created by Eduardo Alvarez on 17/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import UIKit

extension CartView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cartItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartCell

        let orderItem = viewModel.cartItems[indexPath.row]
        let formatedPrice = viewModel.formatItemTotal(price: orderItem.price, quantity: orderItem.quantity)
        let formatedQuantity = viewModel.formatItemUnits(quantity: orderItem.quantity)

        cell.price?.text = formatedPrice
        cell.subtitle?.text = formatedQuantity
        cell.title?.text = orderItem.name
        cell.thumbnail?.sd_setImage(with: URL(string: orderItem.image!), placeholderImage: UIImage(named: "placeholder.png"))

        return cell
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel.removeItemFromCart(index: indexPath.row)
            myTableView.deleteRows(at: [indexPath], with: .fade)

            updateView()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cartItem = viewModel.cartItems[indexPath.row]

        let product  = Product(data: [
            "id": cartItem.id,
            "title": cartItem.name,
            "price": cartItem.price,
            "text": cartItem.description!,
            "image" : cartItem.image!
        ])

        selectedDocument = product

        self.performSegue(withIdentifier: "showProductFromCart", sender: UITableView.self)
    }
}

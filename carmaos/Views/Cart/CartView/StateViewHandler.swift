//
// Created by Eduardo Alvarez on 17/8/18.
// Copyright (c) 2018 Eduardo Alvarez. All rights reserved.
//

import Foundation
import UIKit

extension CartView {

    func setStateMessage() {
        // Inverse If's chain
        // The important items are at the bottom

        stateImage.tintColor = Brand.color_accent
        stateView.removeFromSuperview()

        if (viewModel.isCartEmpty) {
            setCartEmptyState()
            addStateView(stateView: stateView)
        }

        if (viewModel.isStoreClosed) {
            setStoreClosedState()
            addStateView(stateView: stateView)
        }

        if (viewModel.isOrderPlaced) {
            setOrderPlacedState()
            addStateView(stateView: stateView)
        }
    }

    // Refactor move to Model
    func setOrderPlacedState() {
        stateTitle.text = "Tu orden está en camino"
        stateDescription.text = "Puedes ver los detalles de la orden en tu perfil"
        stateImage.image = #imageLiteral(resourceName: "glyph-delivery")
        stateButton.setTitle("Ver Orden", for: .normal)
    }

    func setStoreClosedState() {
        stateTitle.text = "La tienda está cerrada"
        stateDescription.text = "En este momento no podemos atenderte. Mañana estaremos de vuelta."
        stateImage.image = #imageLiteral(resourceName: "glyph-store")
        stateButton.setTitle("Ver Menú", for: .normal)
    }

    func setCartEmptyState() {
        stateTitle.text = "El carrito está vacío"
        stateDescription.text = "Llena el carrito con nuestros platillos y regresa para realizar tu compra."
        stateImage.image = #imageLiteral(resourceName: "glyph-cart")
        stateButton.setTitle("Ver Menú", for: .normal)
    }

    func addStateView(stateView: UIView){
        stateView.frame.size.width = view.safeAreaLayoutGuide.layoutFrame.width
        stateView.frame.size.height = view.safeAreaLayoutGuide.layoutFrame.height
        stateView.frame.origin = CGPoint(x: 0, y: view.safeAreaLayoutGuide.layoutFrame.minY)

        view.addSubview(stateView)
    }

    @IBAction func stateButton(_ sender: UIButton) {
        if (viewModel.isOrderPlaced) {
            // selectedIndex = 2 is Profile
            tabBarController?.selectedIndex = 2
        }
        else {
            // selectedIndex = 0 is Menu
            tabBarController?.selectedIndex = 0
        }
    }
}

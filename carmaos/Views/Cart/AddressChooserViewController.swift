//
//  AddressChooserViewController.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 27/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class AddressChooserViewController: UIViewController {

    var user: User = AppStore.user
    
    @IBOutlet weak var myGlyph: UIImageView!
    @IBOutlet weak var myTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setView()
    }

    func setView() {
        myGlyph.tintColor = Brand.color_accent
        myTableView.tableFooterView = UIView()
        myTableView.alwaysBounceVertical = false
    }
}

extension AddressChooserViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return user.addresses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        let item = user.addresses[indexPath.row]
        
        cell.textLabel?.text = item.urbanization
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        user.selectedAddress = indexPath.row
        
        self.performSegue(withIdentifier: "showPayment", sender: UITableView.self)
    }
}

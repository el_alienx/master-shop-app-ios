//
//  NewAddressViewController.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 13/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class NewAddressViewController: UIViewController {

    private let user = AppStore.user!

    @IBOutlet weak var myGlyph: UIImageView!
    @IBOutlet weak var urbanizationTextField: MSTextField!
    @IBOutlet weak var streetTextField: MSTextField!
    @IBOutlet weak var numerationTextField: MSTextField!
    @IBOutlet weak var referenceTextView: MSTextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setView()
    }
    
    func setView() {
        myGlyph.tintColor = Brand.color_accent
        setForm()
    }

    func setForm() {
        let adr = Address.self
        
        urbanizationTextField.start(title: adr.urbTitle, placeholder: adr.urbPlaceholder, maxLength: 30)
        streetTextField.start(title: adr.stTitle, placeholder: adr.stPlaceholder, maxLength: 35)
        numerationTextField.start(title: adr.numTitle, placeholder: adr.numPlaceholder, maxLength: 30)
        referenceTextView.start(title: adr.refTitle, placeholder: adr.refPlaceholder, maxLength: 140)
        
        urbanizationTextField.required = true
        streetTextField.required = true
        numerationTextField.required = true
    }
    
    @IBAction func mainButton(_ sender: UIButton) {
        if !urbanizationTextField.validate() { return }
        if !streetTextField.validate() { return }
        if !numerationTextField.validate() { return }
        
        let data: [String: Any] = [
            "urbanization": urbanizationTextField.text!,
            "street": streetTextField.text!,
            "numeration": numerationTextField.text!,
            "reference": referenceTextView.text!
        ]

        user.tempAddress = data
        performSegue(withIdentifier: "showPayment", sender: nil)
    }
}

//
//  PaymentViewController.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 13/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {
    
    private let shoppingCart = AppStore.shoppingCart! // Refactor, justify why is this here
    private let restaurant = AppStore.restaurant!
    private let user = AppStore.user!
    private let icons = ["icon-cash", "icon-datafast"]
    
    @IBOutlet weak var myGlyph: UIImageView!
    @IBOutlet weak var myTableView: UITableView!
    
    private var segue: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setView()
    }

    func setView() {
        myGlyph.tintColor = Brand.color_accent
        myTableView.alwaysBounceVertical = false
    }
}

extension PaymentViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurant.paymentMethods!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        
        cell.textLabel?.text = restaurant.paymentMethods![indexPath.row]
        cell.imageView?.image = UIImage(named: icons[indexPath.row])
        cell.imageView?.tintColor = Brand.color_dominant
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let segue = (user.isLogged) ? "showCheckout" : "showAuth"
        
        user.selectedPayment = restaurant.paymentMethods![indexPath.row]
        user.onAuthCompleteSegue = "showCheckout"
        
        self.performSegue(withIdentifier: segue, sender: UITableView.self)
    }
}

//
//  CheckoutViewController.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 13/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

class CheckoutView: UIViewController {
    
    private var viewModel: CheckoutPresenter!

    @IBOutlet weak var myGlyph: UIImageView!
    @IBOutlet weak var commentsTextView: MSTextView!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    
    // Payment details
    @IBOutlet weak var myTotal: UILabel!
    @IBOutlet weak var mySubtotal: UILabel!
    @IBOutlet weak var myShippingCost: UILabel!
    @IBOutlet weak var myTransactionStackView: UIStackView!
    @IBOutlet weak var myTransactionCost: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let shoppingCart = AppStore.shoppingCart!
        let restaurant = AppStore.restaurant!
        let user = AppStore.user!

        viewModel = CheckoutPresenter(model: CheckoutModel(
                shoppingCart: shoppingCart,
                restaurant: restaurant,
                user: user
        ) )
        
        setView()
    }
    
    func setView() {
        myGlyph.tintColor = Brand.color_accent
        commentsTextView.start(title: "Indicaciones",
                placeholder: "La comida sin cebolla ni tomate", maxLength: 140)
        
        setPaymentCost()
    }

    func callBack(success: Bool) -> () {
        viewModel.successStatus = success

        self.performSegue(withIdentifier: "showOrderResult", sender: nil)
    }
    
    func setPaymentCost() {
        let vm = viewModel!

        myTotal.text = vm.showTotal()
        mySubtotal.text = vm.showSubtotal()
        myShippingCost.text = vm.showShipping()
        myTransactionCost.text = vm.showTransactionCost()

        if (vm.showTransactionCost() == nil) {
            myTransactionStackView.removeFromSuperview()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationPage = segue.destination as! OrderResultView
        
        destinationPage.receivedStatus = viewModel.successStatus
    }
    
    @IBAction func mainButton(_ sender: UIButton) {
        viewModel.comments = commentsTextView.getText() ?? "Sin comentarios"
        mainButton.alpha = 0
        myActivityIndicator.startAnimating()
        
        viewModel.placeOrder(callBack: callBack)
    }
}
//
//  CheckoutResultView.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 16/8/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit

final class OrderResultView: UIViewController {

    private var viewModel: CheckoutResultPresenter!
    var receivedStatus: Bool?
    
    @IBOutlet weak var myGlyph: UIImageView!
    @IBOutlet weak var myTitle: UILabel!
    @IBOutlet weak var myDescription: UILabel!
    @IBOutlet weak var myMainButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let status = receivedStatus {
            let shoppingCart: ShoppingCart = AppStore.shoppingCart
            let user: User = AppStore.user

            viewModel = CheckoutResultPresenter(model: CheckoutResultModel(shoppingCart: shoppingCart, user: user))

            setView(status: status)
        }
        else {
            print("OrderResult.swift - No status received.")
        }
    }

    func setView(status: Bool) {
        if (status) { onSuccess() }
        else { onFailure() }
    }
    
    func onSuccess() {
        viewModel.completeOrder()
        tabBarController?.tabBar.items?[1].badgeValue = viewModel.updateCartBadge()
        navigationItem.title = "Orden realizada"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Regresar", style: .plain, target: self, action: #selector(self.backToInitial(sender:)))

        setState(state: viewModel.stateSuccess)
    }
    
    func onFailure() {
        navigationItem.title = "Orden pendiente"

        setState(state: viewModel.stateFail)
    }

    func setState(state: StateMessage) {
        myGlyph.image = #imageLiteral(resourceName: state.image)
        myGlyph.tintColor = Brand.color_accent
        myTitle.text = state.title
        myDescription.text = state.description
        myMainButton.setTitle(state.buttonTitle, for: .normal)
    }
    
    @objc func backToInitial(sender: AnyObject) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func mainButton(_ sender: UIButton) {
        if (receivedStatus!) {
            // selectedIndex = 2 is Profile
            tabBarController?.selectedIndex = 2
        }
        else {
            navigationController?.popViewController(animated: true)
        }
    }
}

//
//  SecondViewController.swift
//  carmaos
//
//  Created by Eduardo Alvarez on 19/7/18.
//  Copyright © 2018 Eduardo Alvarez. All rights reserved.
//

import UIKit
import SDWebImage

final class CartView: UIViewController {
    
    internal var viewModel: CartPresenter!
    internal var selectedDocument: Product?

    // Cart
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myTotalUnits: UILabel!
    @IBOutlet weak var mySubTotal: UILabel!

    // State message
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var stateImage: UIImageView!
    @IBOutlet weak var stateTitle: UILabel!
    @IBOutlet weak var stateDescription: UILabel!
    @IBOutlet weak var stateButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        let shoppingCart = AppStore.shoppingCart!
        let restaurant = AppStore.restaurant!
        let user = AppStore.user!
        
        viewModel = CartPresenter(model: CartModel(
                shoppingCart: shoppingCart,
                restaurant: restaurant,
                user: user)
        )
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        setView()
        updateView()
    }
    
    func delegateCallBack() -> () {        
        setView()
        updateView()
    }
    
    func setView() {
        myTableView.reloadData()
        myTableView.alwaysBounceVertical = false
    }
    
    func updateView() {
        tabBarController?.tabBar.items?[1].badgeValue = viewModel.updateCartTabBadge()
        
        mySubTotal.text = viewModel.formatOrderSubtotal()
        myTotalUnits.text = viewModel.formatOrderTotalUnits()
        myTableView.tableFooterView = UIView()
        
        setStateMessage()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showProductFromCart" {
            let destinationPage = segue.destination as! ProductView

            destinationPage.receivedDocument = selectedDocument
            destinationPage.receivedCallback = delegateCallBack
        }
    }
    
    @IBAction func mainButton(_ sender: UIButton) {
        let condition: Bool = viewModel.isUserLogged && viewModel.addresses.count > 0
        let segue = condition ? "showAddresses" : "showNewAddress"

        self.performSegue(withIdentifier: segue, sender: nil)
    }
}

// Contains the following extensions
// TableView.swift to handle the table view cells of the shopping cart
// StateViewHandler.swift to handle the different status screen

//
//  IntroView
//  
//
//  Created by Eduardo Alvarez on 3/8/18.
//

import UIKit

final class IntroView: UIViewController {
    
    private var viewModel: IntroPresenter!
    private let notification = Notification.Name(rawValue: menuNotificationKey)
    
    @IBOutlet weak var welcomeView: UIView!
    @IBOutlet weak var notificationsView: UIView!
    @IBOutlet weak var cacheTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let menu = AppStore.menu!
        let user = AppStore.user!

        viewModel = IntroPresenter(model: IntroModel(menu: menu, user: user))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // The dispatch segue in setView
        // won't work on viewDidLoad()
        if viewModel.isReturning {
            dispatchSegue()
        }
        else {
            viewModel.isReturning = true
            addStateView(stateView: welcomeView)
            createObserver()
        }
    }
    
    func createObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(IntroView.cacheImages), name: notification, object: nil)
    }
    
    func addStateView(stateView: UIView){
        stateView.frame.size.width = view.safeAreaLayoutGuide.layoutFrame.width
        stateView.frame.size.height = view.safeAreaLayoutGuide.layoutFrame.height
        stateView.frame.origin = CGPoint(x: 0, y: view.safeAreaLayoutGuide.layoutFrame.minY)
        
        view.addSubview(stateView)
    }
    
    @objc func cacheImages() {
        // You have to add the view before reloading data,
        // otherwise the SDWebImage optimization block the download
        view.addSubview(cacheTable)
        
        cacheTable.alpha = 0
        cacheTable.reloadData()
    }
    
    func dispatchSegue() {
        self.performSegue(withIdentifier: "showTabBarController", sender: nil)
    }
}

extension IntroView {
    @IBAction func welcomeButton(_ sender: UIButton) {
        welcomeView.removeFromSuperview()
        addStateView(stateView: notificationsView)
    }
    
    @IBAction func notificationsButton(_ sender: UIButton) {
        AppStore.notificationDialog.registerRemoteNotifications()
        dispatchSegue()
    }
}

extension IntroView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.collection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        let item = viewModel.collection[indexPath.row]
        
        cell.textLabel?.text = item.title
        cell.imageView?.sd_setImage(with: URL(string: item.image), placeholderImage: UIImage(named: "placeholder.png"))
        
        return cell
    }
}
